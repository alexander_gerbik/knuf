from unittest import mock

import aiopg
import pytest

import config
from models import Song
from room import Room


@pytest.fixture
async def db(event_loop):
    pool = await aiopg.create_pool(config.DSN, loop=event_loop)

    yield pool

    pool.close()
    await pool.wait_closed()


@pytest.fixture
async def room(db, request):
    room_fade_time = getattr(request.function, "room_fade_time", 10)
    room = await Room.create('ben', 'qwe', room_fade_time, False, db)
    yield room
    await room.change_state(None)


@pytest.fixture
async def playing_state(room, request):
    song_duration = getattr(request.function, "song_duration", 5)

    a = Client('ben', 1)
    b = Client('dover', 2)

    await a.connect(room)
    s = Song(1, 'let the storm descend upon you', 'avantasia', song_duration,
             'some_url')
    await a.send_message(room, {'type': 'playlist_add', **s.to_dict()})

    await b.connect(room)

    return Namespace(leader=a, other_player=b, song=s, room=room)


@pytest.fixture
async def fading_state(playing_state):
    room = playing_state.room

    a = playing_state.leader
    b = playing_state.other_player

    c = Client('matt', 3)
    await c.connect(room)

    await b.send_message(room,
                         {'type': 'guess', 'song_id': playing_state.song.id})
    s = Song(1, 'another angel down', 'avantasia', 5, 'another_url')
    await b.send_message(room, {'type': 'playlist_add', **s.to_dict()})

    return Namespace(leader=a, next=b, third_player=c, song=playing_state.song,
                     next_song=s, room=room)


class Client:
    def __init__(self, name, uid):
        self.name = name
        self.uid = uid
        self.messages = []

    @property
    def ws(self):
        return self

    async def send_json(self, message):
        self.messages.append(message)

    async def connect(self, room):
        await room.connect(self.ws, self.name, self.uid)

    async def disconnect(self, room):
        await room.disconnect(self.uid)

    async def send_message(self, room, message):
        await room.dispatch(self.uid, message)


def AsyncMock(*args, **kwargs):
    m = mock.MagicMock(*args, **kwargs)

    async def mock_coro(*args, **kwargs):
        return m(*args, **kwargs)

    mock_coro.mock = m
    return mock_coro


class _Whatever:
    def __eq__(self, other):
        return True


whatever = _Whatever()


class Namespace:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


def attrs(**kwargs):
    def inner(function):
        function.__dict__.update(kwargs)
        return function

    return inner
