from models import Playlist, Song


def test_playlist():
    playlist = Playlist()

    assert not playlist

    a = Song(1, 'forever', 'kamelot', 15)
    b = Song(2, 'the scarecrow', 'avantasia', 15)
    c = Song(3, 'amaranth', 'draconian', 17)

    playlist.append(a)

    assert playlist

    playlist.append(b)
    playlist.append(c)

    playlist.remove(2)

    assert 2 not in playlist
    assert 1 in playlist
    assert 3 in playlist

    d = playlist.pop()
    assert d is a

    e = playlist.pop()
    assert e is c

    assert playlist.pop() is None


def test_add_song_multiple_times__only_one_track():
    playlist = Playlist()
    song = Song(1, 'forever', 'kamelot', 15)

    playlist.append(song)
    playlist.append(song)

    assert len(playlist) == 1

