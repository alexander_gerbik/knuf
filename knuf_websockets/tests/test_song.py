from unittest.mock import patch

import pytest

from models import Song
from tests.conftest import AsyncMock


@pytest.mark.asyncio
async def test_mangle_songs_contains_target_song():
    song = Song(4, 'so cold', 'breaking benjamin', 22)

    with patch('models.Song._random_songs_same_genre', new=AsyncMock()) as coro:
        coro.mock.return_value = [
            Song(1, 'forever', 'kamelot', 15),
            Song(2, 'the scarecrow', 'avantasia', 15),
            Song(3, 'amaranth', 'draconian', 17),
        ]

        songs = await song.mangle(3, 'connection')

    assert len([1 for s in songs if s.id == song.id]) == 1


@pytest.mark.asyncio
async def test_mangle_songs_contains_target_song_only_once():
    song = Song(1, 'so cold', 'breaking benjamin', 22)

    with patch('models.Song._random_songs_same_genre', new=AsyncMock()) as coro:
        coro.mock.return_value = [
            Song(1, 'so cold', 'breaking benjamin', 22),
            Song(2, 'the scarecrow', 'avantasia', 15),
            Song(3, 'amaranth', 'draconian', 17),
        ]

        songs = await song.mangle(3, 'connection')

    assert len([1 for s in songs if s.id == song.id]) == 1


@pytest.mark.asyncio
async def test_mangle_not_enough_song_in_db__runtime_error():
    song = Song(1, 'so cold', 'breaking benjamin', 22)

    with patch('models.Song._random_songs_same_genre', new=AsyncMock()) as cor:
        cor.mock.return_value = [
            Song(3, 'amaranth', 'draconian', 17),
        ]

        with pytest.raises(RuntimeError, match='.*not enough songs.*'):
            await song.mangle(3, 'connection')
