import asyncio

import pytest
import time
from pytest import raises

from tests.conftest import AsyncMock
from utils import Substitute, DelayableAsyncTimer


def test_substitute_single_type():
    with raises(TypeError):
        with Substitute(TypeError, ValueError):
            raise ValueError()


def test_dont_intercept_other_exceptions():
    with raises(RuntimeError):
        with Substitute(TypeError, ValueError):
            raise RuntimeError()


def test_saves_original_exception():
    with raises(TypeError) as exc:
        with Substitute(TypeError, ValueError):
            expected = ValueError()
            raise expected

    got = exc.value.__cause__
    assert got == expected


def test_discard_original_exception():
    with raises(TypeError) as exc:
        with Substitute(TypeError, ValueError, chain=False):
            expected = ValueError()
            raise expected

    got = exc.value.__cause__
    assert got is None


def test_substitute_multiple_types():
    with raises(TypeError):
        with Substitute(TypeError, (ValueError, RuntimeError)):
            raise ValueError()

    with raises(TypeError):
        with Substitute(TypeError, (ValueError, RuntimeError)):
            raise RuntimeError()


@pytest.mark.asyncio
async def test_sleep_monotonic_same_units():
    start = time.monotonic()
    await asyncio.sleep(1)
    end = time.monotonic()
    assert pytest.approx(end - start, rel=0.01) == 1


@pytest.mark.slow
@pytest.mark.asyncio
async def test_timer_delayed__callback_called_later():
    timeout = 2
    delay = 1

    coro = AsyncMock()
    timer = DelayableAsyncTimer(timeout, coro)
    timer.start()

    timer.advance(delay)
    await asyncio.sleep(timeout)
    coro.mock.assert_not_called()

    await asyncio.sleep(delay)
    coro.mock.assert_called_once()
