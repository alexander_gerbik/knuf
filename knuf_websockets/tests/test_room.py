import asyncio
import logging
from unittest.mock import patch, call

import pytest
from pytest import approx

import config
from models import Song
from room import Room
from states import FadeState, PlayingState, PendingState, IdleState
from tests.conftest import Client, AsyncMock, whatever, attrs


@pytest.mark.asyncio
async def test_broadcast__all_user_get_message(room):
    clients = [
        Client('ben', 1),
        Client('dover', 2),
        Client('matt', 3),
        Client('dick', 4)
    ]

    for client in clients:
        await client.connect(room)

    message = {'type': 'some_type', 'payload': 'some_payload'}

    await room.send(Room.BROADCAST, message)

    for client in clients:
        assert client.messages[-1] == message


@pytest.mark.asyncio
async def test_send__only_recipient_get_message(room):
    ben = Client('ben', 1)
    dover = Client('dover', 2)
    await ben.connect(room)
    await dover.connect(room)
    message = {'type': 'some_type', 'payload': 'some_payload'}

    await room.send(1, message)

    assert ben.messages[-1] == message
    assert dover.messages[-1] != message


@pytest.mark.asyncio
async def test_two_users_no_songs__state_pending(room):
    ben = Client('ben', 1)
    dover = Client('dover', 2)

    assert isinstance(room._state, IdleState)

    await ben.connect(room)
    assert isinstance(room._state, PendingState)

    await dover.connect(room)
    assert isinstance(room._state, PendingState)


@pytest.mark.asyncio
async def test_user_add_song_before_connect__state_playing(room):
    ben = Client('ben', 1)
    dover = Client('dover', 2)

    assert isinstance(room._state, IdleState)

    await ben.connect(room)
    assert isinstance(room._state, PendingState)

    song = Song(1, 'avantasia', 'avantasia', 55, 'some_url')
    await ben.send_message(room, {'type': 'playlist_add', **song.to_dict()})
    assert isinstance(room._state, PendingState)

    await dover.connect(room)
    assert isinstance(room._state, PlayingState)
    assert room._state.uid == ben.uid
    assert room._state.song == song


@pytest.mark.asyncio
async def test_user_add_song_after_connect__state_playing(room):
    ben = Client('ben', 1)
    dover = Client('dover', 2)

    assert isinstance(room._state, IdleState)

    await ben.connect(room)
    assert isinstance(room._state, PendingState)

    await dover.connect(room)
    assert isinstance(room._state, PendingState)

    song = Song(1, 'avantasia', 'avantasia', 55, 'some_url')
    await dover.send_message(room, {'type': 'playlist_add', **song.to_dict()})
    assert isinstance(room._state, PlayingState)
    assert room._state.uid == dover.uid
    assert room._state.song == song


@pytest.mark.asyncio
async def test_preserve_points_and_playlist_between_connections(room):
    uid = 1
    ben = Client('ben', uid)
    song_id = 1
    song = Song(song_id, 'forever', 'kamelot', 15)
    await ben.connect(room)

    points = 15
    scores = 27
    room.players[uid].score = scores
    room.players[uid].points = points
    await ben.send_message(room, {'type': 'playlist_add', **song.to_dict()})

    await ben.disconnect(room)

    assert uid not in set(room.connected_players())

    await ben.connect(room)

    assert room.players[uid].score == scores
    assert room.players[uid].points == points
    assert song_id in room.players[uid].playlist


@pytest.mark.asyncio
@pytest.mark.slow
async def test_after_idle_seconds__room_closes(db):
    seconds = 2

    back_up = config.IDLE_SECONDS
    config.IDLE_SECONDS = seconds

    room = await Room.create('ben', 'qwe', 10, False, db)

    callback = AsyncMock()
    room.closed.connect(callback)

    await asyncio.sleep(seconds + 1)

    config.IDLE_SECONDS = back_up

    assert room._state is None
    callback.mock.assert_called_once()


@pytest.mark.asyncio
@pytest.mark.slow
async def test_song_ends__change_song(room, caplog):
    caplog.set_level(logging.DEBUG)
    duration = 2

    ben = Client('ben', 1)
    dover = Client('dover', 2)

    await ben.connect(room)
    a = Song(1, 'avantasia', 'avantasia', duration, 'some_url')
    await ben.send_message(room, {'type': 'playlist_add', **a.to_dict()})
    b = Song(2, 'without you', 'breaking benjamin', duration, 'another_url')
    await ben.send_message(room, {'type': 'playlist_add', **b.to_dict()})

    await dover.connect(room)
    c = Song(3, 'lights out', 'breaking benjamin', duration, 'url')
    await dover.send_message(room, {'type': 'playlist_add', **c.to_dict()})

    assert isinstance(room._state, PlayingState)
    assert room._state.uid == ben.uid
    assert room._state.song == a

    await asyncio.sleep(duration + 1)

    assert isinstance(room._state, PlayingState)
    assert room._state.uid == ben.uid
    assert room._state.song == b

    await asyncio.sleep(duration + 0.5)

    assert isinstance(room._state, PlayingState)
    assert room._state.song == c
    assert room._state.uid == dover.uid

    await asyncio.sleep(duration)

    assert isinstance(room._state, PendingState)


@pytest.mark.asyncio
async def test_message__all_get_message(room):
    ben = Client('ben', 1)
    dover = Client('dover', 2)

    message = 'some message from ben'

    await ben.connect(room)
    await dover.connect(room)

    with patch.object(Room, 'send', new=AsyncMock()) as coro:
        await ben.send_message(room, {'type': 'message', 'payload': message})

    expected = {
        'type': 'message',
        'payload': message,
        'sender_id': 1,
        'sender': 'ben',
        'timestamp': whatever,
    }
    coro.mock.assert_called_once_with(room, Room.BROADCAST, expected)


@pytest.mark.asyncio
async def test_playlist(room):
    uid = 1
    ben = Client('ben', uid)

    await ben.connect(room)

    a = Song(1, 'avantasia', 'avantasia', 15, 'some_url')
    b = Song(2, 'without you', 'breaking benjamin', 15, 'another_url')
    c = Song(3, 'lights out', 'breaking benjamin', 15, 'url')

    await ben.send_message(room, {'type': 'playlist_add', **a.to_dict()})
    await ben.send_message(room, {'type': 'playlist_add', **b.to_dict()})
    await ben.send_message(room, {'type': 'playlist_add', **c.to_dict()})

    assert a in room.players[uid].playlist
    assert b in room.players[uid].playlist
    assert c in room.players[uid].playlist

    await ben.send_message(room, {'type': 'playlist_remove', 'song_id': 2})

    assert a in room.players[uid].playlist
    assert b not in room.players[uid].playlist
    assert c in room.players[uid].playlist


@pytest.mark.asyncio
@pytest.mark.slow
async def test_users_get_information_about_current_song(playing_state):
    song = playing_state.song
    expected = {
        'type': 'state_enter',
        'state': 'PlayingState',
        'uid': playing_state.leader.uid,
        'url': song.url,
        'from': approx(0, abs=0.03),
        'to': song.duration,
        'choices': whatever,
        'allow_skip': False,
    }

    assert playing_state.leader.messages[-1] == expected
    assert playing_state.other_player.messages[-1] == expected

    await asyncio.sleep(2)

    matt = Client('matt', 3)
    expected.update(**{'from': approx(2, abs=0.03)})
    await matt.connect(playing_state.room)
    assert matt.messages[-1] == expected


@pytest.mark.asyncio
@pytest.mark.slow
async def test_user_guess_right__state_change_add_points(playing_state):
    song = playing_state.song
    leader = playing_state.leader
    player = playing_state.other_player
    room = playing_state.room

    been_played = 2
    winner_score = approx(song.duration - been_played, 0.03)
    leader_score = approx(been_played * config.LEADER_MULTIPLIER, 0.03)

    await asyncio.sleep(been_played)

    with patch.object(Room, 'send', new=AsyncMock()) as coro:
        await player.send_message(room, {'type': 'guess', 'song_id': song.id})

    coro.mock.assert_has_calls([
        call(room, Room.BROADCAST, {
            'type': 'update_user',
            'name': player.name,
            'uid': player.uid,
            'points': winner_score,
            'score': winner_score,
            'connected': False,
            'disconnected': False,
        }),
        call(room, Room.BROADCAST, {
            'type': 'guess_result',
            'uid': player.uid,
            'success': True,
            'song_id': song.id,
        }),
        call(room, Room.BROADCAST, {
            'type': 'update_user',
            'name': leader.name,
            'uid': leader.uid,
            'points': leader_score,
            'score': leader_score,
            'connected': False,
            'disconnected': False,
        }),
    ], any_order=True)

    assert winner_score == room.players[player.uid].points
    assert leader_score == room.players[leader.uid].points
    assert isinstance(room._state, FadeState)
    assert room._state.next_uid == player.uid


@pytest.mark.asyncio
@pytest.mark.slow
async def test_user_guess_wrong__state_points_same(playing_state):
    song = playing_state.song
    leader = playing_state.leader
    player = playing_state.other_player
    room = playing_state.room

    with patch.object(Room, 'send', new=AsyncMock()) as coro:
        await player.send_message(room,
                                  {'type': 'guess', 'song_id': song.id + 1})

    coro.mock.assert_any_call(room, Room.BROADCAST, {
        'type': 'guess_result',
        'song_id': song.id + 1,
        'uid': player.uid,
        'success': False,
    })

    assert room.players[player.uid].points == 0
    assert room.players[leader.uid].points == 0
    assert isinstance(room._state, PlayingState)
    assert room._state.uid == leader.uid
    assert room._state.song == song
    assert player.uid in room._state.losers


@pytest.mark.asyncio
async def test_only_if_all_users_guess_wrong__skip_allowed(playing_state):
    song = playing_state.song
    room = playing_state.room
    leader = playing_state.leader
    player = playing_state.other_player
    c = Client('lex', 13)
    d = Client('kate', 14)
    await c.connect(room)
    await d.connect(room)

    assert room._state.allow_skip == False

    await player.send_message(room, {'type': 'guess', 'song_id': song.id + 1})

    assert room._state.allow_skip == False

    await c.send_message(room, {'type': 'guess', 'song_id': song.id + 1})

    with patch.object(Room, 'send', new=AsyncMock()) as coro:
        await d.send_message(room, {'type': 'guess', 'song_id': song.id + 1})

    coro.mock.assert_any_call(room, leader.uid, {'type': 'allow_skip'})
    assert room._state.allow_skip == True


@pytest.mark.asyncio
async def test_skip__next_song_playing(playing_state):
    song = playing_state.song
    room = playing_state.room
    leader = playing_state.leader
    player = playing_state.other_player

    s = Song(2, 'lucifer', 'avantasia', 12, 'some_url')
    await leader.send_message(room, {'type': 'playlist_add', **s.to_dict()})

    await player.send_message(room, {'type': 'guess', 'song_id': song.id + 1})

    await leader.send_message(room, {'type': 'skip'})

    assert isinstance(room._state, PlayingState)
    assert room._state.allow_skip == False
    assert room._state.song == s


@pytest.mark.asyncio
async def test_skip__leader_gets_points_for_whole_duration(playing_state):
    song = playing_state.song
    room = playing_state.room
    leader = playing_state.leader
    player = playing_state.other_player

    start_score = room.players[leader.uid].score

    await player.send_message(room, {'type': 'guess', 'song_id': song.id + 1})

    await leader.send_message(room, {'type': 'skip'})

    end_score = room.players[leader.uid].score
    expected_got_score = approx(end_score - start_score, 0.00001)
    assert expected_got_score == song.duration * config.LEADER_MULTIPLIER


@pytest.mark.asyncio
@pytest.mark.slow
async def test_user_guess_wrong_then_right__points_state_same(playing_state):
    song = playing_state.song
    leader = playing_state.leader
    player = playing_state.other_player
    room = playing_state.room

    await player.send_message(room, {'type': 'guess', 'song_id': song.id + 1})

    await player.send_message(room, {'type': 'guess', 'song_id': song.id})

    assert room.players[player.uid].points == 0
    assert room.players[leader.uid].points == 0
    assert isinstance(room._state, PlayingState)
    assert room._state.uid == leader.uid
    assert room._state.song == song


@pytest.mark.asyncio
@pytest.mark.slow
@attrs(song_duration=5, room_fade_time=1)
async def test_fade_timed_out__next_round_starts(fading_state):
    room = fading_state.room
    assert isinstance(room._state, FadeState)

    await asyncio.sleep(2)

    assert isinstance(room._state, PlayingState)
    assert room._state.uid == fading_state.next.uid
    assert room._state.song == fading_state.next_song


@pytest.mark.asyncio
@pytest.mark.slow
@attrs(song_duration=1, room_fade_time=3)
async def test_song_ends__next_round_starts(fading_state):
    room = fading_state.room
    assert isinstance(room._state, FadeState)

    await asyncio.sleep(0.9)

    assert isinstance(room._state, FadeState)

    await asyncio.sleep(0.5)

    assert isinstance(room._state, PlayingState)
    assert room._state.uid == fading_state.next.uid
    assert room._state.song == fading_state.next_song


@pytest.mark.asyncio
@pytest.mark.slow
@attrs(song_duration=1, room_fade_time=3)
async def test_not_enough_points__nothing_happens(fading_state):
    room = fading_state.room
    player = fading_state.next

    expected = room._state.deadline
    await player.send_message(room, {'type': 'pay', 'seconds': 5})

    assert room._state.deadline == expected


@pytest.mark.asyncio
@pytest.mark.slow
@attrs(song_duration=5, room_fade_time=1)
async def test_pay__advances_deadline(fading_state):
    room = fading_state.room
    player = fading_state.leader
    await room.add_points(player.uid, 10 * config.PAY_MULTIPLIER)

    starting_deadline = room._state.deadline
    await player.send_message(room, {'type': 'pay', 'seconds': 2})

    await asyncio.sleep(2)

    await player.send_message(room, {'type': 'pay', 'seconds': 10})

    expected_deadline = approx(starting_deadline + (5 - 1), abs=0.01)
    assert expected_deadline == room._state.deadline

    await asyncio.sleep(2.99)

    assert isinstance(room._state, FadeState)

    await asyncio.sleep(0.02)

    assert isinstance(room._state, PlayingState)
    assert room._state.uid == fading_state.next.uid


@pytest.mark.asyncio
@pytest.mark.slow
@attrs(song_duration=7, room_fade_time=3)
async def test_pay__spends_points(fading_state):
    room = fading_state.room
    player = fading_state.next
    await room.add_points(player.uid, 10 * config.PAY_MULTIPLIER)
    starting_points = room.players[player.uid].points

    await player.send_message(room, {'type': 'pay', 'seconds': 3})

    assert (room.players[player.uid].points
            == starting_points - 3 * config.PAY_MULTIPLIER)


@pytest.mark.asyncio
@attrs(song_duration=27, room_fade_time=5)
async def test_pay_to_much__spends_points_as_needed(fading_state):
    room = fading_state.room
    player = fading_state.next
    await room.add_points(player.uid, 30 * config.PAY_MULTIPLIER)
    starting_points = room.players[player.uid].points

    starting_deadline = room._state.deadline
    await player.send_message(room, {'type': 'pay', 'seconds': 28})

    assert approx(starting_deadline + (27 - 5), 0.01) == room._state.deadline

    exected_points = approx(starting_points - (27 - 5) * config.PAY_MULTIPLIER,
                            rel=0.01)
    assert (exected_points == room.players[player.uid].points)
