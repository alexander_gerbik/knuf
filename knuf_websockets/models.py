import random

from collections import deque


class Playlist:
    def __init__(self):
        self._registry = {}
        self._tracks = deque()

    def __bool__(self):
        return bool(self._tracks)

    def __iter__(self):
        return iter(self._tracks)

    def __getitem__(self, song_id):
        return self._registry[song_id]

    def __contains__(self, item):
        if isinstance(item, Song):
            return item.id in self._registry
        return item in self._registry

    def __len__(self):
        return len(self._tracks)

    def append(self, song):
        if song in self._tracks:
            return
        self._registry[song.id] = song
        self._tracks.append(song)

    def pop(self):
        try:
            song = self._tracks.popleft()
        except IndexError:
            return None
        self._registry.pop(song.id)
        return song

    def remove(self, song_id):
        song = self._registry[song_id]
        self._registry.pop(song_id)
        self._tracks.remove(song)
        return song


class Player:
    def __init__(self, name, playlist=None, score=0, points=0, ws=None):
        if playlist is None:
            playlist = Playlist()
        self.ws = ws
        self.name = name
        self.playlist = playlist
        self.score = score
        self.points = points

    def add_points(self, points):
        if points < 0:
            raise ValueError('points can\'t be negative')
        self.score += points
        self.points += points

    def withdraw(self, points):
        if points < 0:
            raise ValueError('points can\'t be negative')
        if self.points < points:
            return False
        self.points -= points
        return True


class Song:
    def __init__(self, id, name, artist, duration, url=None, begin=None, end=None):
        self.name = name
        self.artist = artist
        self.url = url
        self.id = int(id)
        self.duration = int(duration)
        self.begin = 0 if begin is None else float(begin)
        self.end = self.duration if end is None else float(end)

    def __str__(self):
        return '{} - {}'.format(self.artist, self.name)

    def __repr__(self):
        return '<{} {} {}>'.format(
            self.__class__.__name__, self.id, self.name)

    def __eq__(self, other):
        if not isinstance(other, Song):
            return NotImplemented
        return self.id == other.id

    def to_dict(self):
        return self.__dict__.copy()

    async def mangle(self, n, db_connection):
        random_songs = await self._random_songs_same_genre(n, db_connection)
        n = min(n, len(random_songs))
        if n < 2:
            raise RuntimeError('Got not enough songs from database to succeed')
        if self.id in set(s.id for s in random_songs):
            return random_songs
        random_songs[random.randrange(len(random_songs))] = self
        return random_songs

    async def _random_songs_same_genre(self, n, db_connection):
        async with db_connection.cursor() as cur:
            await cur.execute(_random_songs_same_genre_sql, (self.id, n))
            result = [Song(*row) async for row in cur]
        more = n - len(result)
        if more:
            result.extend(await self._random_songs(more, db_connection))
        return result

    async def _random_songs(self, n, db_connection):
        async with db_connection.cursor() as cur:
            await cur.execute(_random_songs_sql, (n,))
            return [Song(*row) async for row in cur]


_random_songs_sql = """
SELECT
  music_song.id,
  music_song.name,
  music_artist.name AS artist,
  duration
FROM music_song, music_artist WHERE artist_id = music_artist.id
ORDER BY random() LIMIT %s;
"""

_random_songs_same_genre_sql = """
SELECT
  music_song.id,
  music_song.name,
  music_artist.name AS artist,
  music_song.duration
FROM music_song, music_artist
WHERE music_song.artist_id = music_artist.id AND
      music_song.id IN (
        SELECT DISTINCT b.song_id
        FROM music_song_genres AS a, music_song_genres AS b
        WHERE a.song_id = %s AND a.genre_id = b.genre_id
      )
ORDER BY random() LIMIT %s;
"""
