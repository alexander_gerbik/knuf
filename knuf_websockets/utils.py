import asyncio

import time


class AsyncTimer:
    def __init__(self, timeout, callback):
        self._timeout = timeout
        self._callback = callback
        self._task = None

    def start(self):
        self._task = asyncio.ensure_future(self._job())

    def cancel(self):
        if self._task:
            self._task.cancel()

    async def _job(self):
        await asyncio.sleep(self._timeout)
        self._task = None
        await self._callback()


class DelayableAsyncTimer:
    def __init__(self, timeout, callback):
        self._deadline = time.monotonic() + timeout
        self._callback = callback
        self._task = None

    def start(self):
        self._task = asyncio.ensure_future(self._job())

    def cancel(self):
        if self._task:
            self._task.cancel()

    async def _job(self):
        while True:
            delta = self._deadline - time.monotonic()
            if delta <= 0:
                break
            await asyncio.sleep(delta)
        self._task = None
        await self._callback()

    def advance(self, delay):
        if delay < 0:
            raise ValueError('delay should be not negative')
        self._deadline += delay


class AsyncEvent(object):
    def __init__(self):
        self._receivers = set()

    def connect(self, receiver):
        self._receivers.add(receiver)

    def disconnect(self, receiver):
        self._receivers.discard(receiver)

    async def send(self, *args, **kwargs):
        res = None
        for receiver in self._receivers.copy():
            res = await receiver(*args, **kwargs)
        return res

    async def __call__(self, *args, **kwargs):
        return await self.send(*args, **kwargs)


class Substitute:
    """Context manager to raise specified exception instead listed
    """

    def __init__(self, target_exception, exceptions, chain=True):
        self._target = target_exception
        self._chain = chain
        self._exceptions = exceptions

    def __enter__(self):
        pass

    def __exit__(self, exctype, excinst, exctb):
        if exctype is not None:
            if issubclass(exctype, self._exceptions):
                cause = excinst if self._chain else None
                raise self._target() from cause
