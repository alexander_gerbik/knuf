from decouple import config

SECRET_KEY = config('SECRET_KEY')
DSN = config('DATABASE_URL')
IDLE_SECONDS = config('IDLE_SECONDS', cast=int)
SONG_CHOICES = config('SONG_CHOICES', cast=int)
LEADER_MULTIPLIER = config('LEADER_MULTIPLIER', cast=float)
PAY_MULTIPLIER = config('PAY_MULTIPLIER', cast=float)
LAST_MESSAGES_SIZE = config('LAST_MESSAGES_SIZE', cast=int)
