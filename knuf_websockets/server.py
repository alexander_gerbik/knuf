import os

import aiohttp
import aiopg
import jwt
from aiohttp import web
from aiohttp.web_exceptions import HTTPNotFound
from json import loads

from aiohttp_wsgi import WSGIHandler

from room import Room
from utils import Substitute
import config

import logging

from knuf.wsgi import application



def init(argv):
    app = web.Application()
    app['rooms'] = {}
    app['next_room_id'] = 1
    app.router.add_post('/create/', create_room)
    app.router.add_get('/rooms/', list_rooms)
    app.router.add_get('/ws/room/{id}/', join_room)

    wsgi_handler = WSGIHandler(application)
    app.router.add_route('*', '/{path_info:.*}', wsgi_handler)

    app.on_startup.append(init_pg)

    return app


async def init_pg(app):
    pool = await aiopg.create_pool(config.DSN, loop=app.loop)
    app['db'] = pool

    async def close_pg(app):
        app['db'].close()
        await app['db'].wait_closed()

    app.on_cleanup.append(close_pg)


async def list_rooms(request):
    rooms = [{'id': id, **room.to_dict()}
             for id, room in request.app['rooms'].items()]
    return web.json_response({'rooms': rooms})


async def create_room(request):
    data = await request.post()
    data = normalize(data)
    id = append_room(request.app, await Room.create(db=app['db'], **data))
    return web.json_response({'id': id})


async def join_room(request):
    with Substitute(HTTPNotFound, (ValueError, KeyError)):
        room = request.app['rooms'][int(request.match_info['id'])]

    ws = web.WebSocketResponse(heartbeat=40)
    await ws.prepare(request)

    username, uid = authenticate(await ws.receive_json(), config.SECRET_KEY)
    if username is None:
        return ws

    await room.connect(ws, username, uid)
    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            await room.dispatch(uid, loads(msg.data))
    await room.disconnect(uid)

    return ws


def append_room(app, room):
    class RoomLoggerAdapter(logging.LoggerAdapter):
        def process(self, msg, kwargs):
            return '[%s] %s' % (self.extra['room'], msg), kwargs

    id = app['next_room_id']
    app['next_room_id'] = id + 1

    logger = logging.getLogger('room')
    logger = RoomLoggerAdapter(logger, {'room': id})
    room.logger = logger

    async def remove_room():
        del app['rooms'][id]

    room.closed.connect(remove_room)
    app['rooms'][id] = room
    return id


def normalize(data):
    res = {
        'creator': int(data['creator']),
        'name': data['name'],
        'fade_time': int(data['fade_time']),
        'private': data['private'] == 'True',
        'genres': [int(g) for g in data['genres']] if 'genres' in data else [],
    }
    return res


def authenticate(d, key):
    token = d['payload']
    try:
        creds = jwt.decode(token, key)
    except jwt.InvalidTokenError:
        return None, None
    return creds['username'], creds['id']


if __name__ == '__main__':
    port = os.environ.get('PORT') or 8000
    app = init([])
    web.run_app(app, port=port)
