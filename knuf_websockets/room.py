import random

import logging
from collections import deque

import config
from utils import AsyncEvent


class Room:
    # use BROADCAST as uid param of 'send' function to broadcast
    BROADCAST = object()

    def __init__(self, creator, name, fade_time, private, db, genres=None):
        self._creator = creator
        self._name = name
        self._genres = genres if genres is not None else []
        self.fade_time = fade_time
        self._private = private
        self.players = {}
        self._state = None
        self._last_messages = deque([], maxlen=config.LAST_MESSAGES_SIZE)
        self.db = db
        self.closed = AsyncEvent()
        self.logger = logging.getLogger(__name__)

    def connected_players(self):
        for p in self.players:
            if self.players[p].ws is not None:
                yield p

    def connected_players_amount(self):
        return sum(1 for _ in self.connected_players())

    @classmethod
    async def create(cls, creator, name, fade_time, private, db, genres=None):
        room = Room(creator, name, fade_time, private, db, genres)

        async def remove():
            await room.change_state(None)

        room.closed.connect(remove)
        from states import IdleState
        await room.change_state(IdleState)
        return room

    def to_dict(self):
        return {
            'creator': self._creator,
            'name': self._name,
            'genres': self._genres,
            'fade_time': self.fade_time,
            'private': self._private
        }

    async def change_state(self, new_state, **kwargs):
        if self._state is not None:
            await self._state.exit()
        if new_state is not None:
            self._state = await new_state.create(self, **kwargs)
        else:
            self._state = None

    def get_random_leader(self):
        """Return random user that can be a leader (has non-empty playlist).

        :return:
        int: The id of chosen player. None if there is no appropriate player.
        """
        player_ids = [id for id, p in self.players.items()
                      if p.playlist and p.ws is not None]
        if not player_ids:
            return None
        return random.choice(player_ids)

    async def send_player_info(self, receiver_uid, uid, connected=False,
                               disconnected=False):
        player = self.players[uid]
        await self.send(receiver_uid, {
            'type': 'update_user',
            'uid': uid,
            'name': player.name,
            'connected': connected,
            'disconnected': disconnected,
            'points': player.points,
            'score': player.score,
        })

    async def notify_playlist_changed(self, uid):
        player = self.players[uid]
        songs = [song.to_dict() for song in player.playlist]
        await self.send(uid, {
            'type': 'update_playlist',
            'playlist': songs,
        })

    async def add_points(self, uid, points):
        player = self.players[uid]
        player.add_points(points)
        await self.send_player_info(self.BROADCAST, uid)

    async def withdraw(self, uid, points):
        player = self.players[uid]
        success = player.withdraw(points)
        if success:
            await self.send_player_info(self.BROADCAST, uid)
        return success

    async def info(self, uid, info):
        await self.send(uid, {'type': 'info', 'payload': info})

    async def send(self, uid, message):
        if uid == self.BROADCAST:
            self.logger.debug('Broadcast: %s', message)
            for uid in self.connected_players():
                await self.players[uid].ws.send_json(message)
        else:
            self.logger.debug('Send to %s: %s', uid, message)
            ws = self.players[uid].ws
            if ws is not None:
                await ws.send_json(message)
            else:
                self.logger.warning('Tried to send message %s to '
                                    'disconnected user %s', message, uid)

    async def connect(self, ws, name, uid):
        await self._state.connect(ws, name, uid)

    async def disconnect(self, uid):
        await self._state.disconnect(uid)

    async def dispatch(self, uid, message):
        await self._state.dispatch(uid, message)
