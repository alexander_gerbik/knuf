from room import Room
import states


class PendingState(states.BaseRoomState):
    async def enter(self):
        t = await super().enter()
        if t is not None:
            return t
        if self._room.connected_players_amount() > 1:
            await self._room.info(Room.BROADCAST,
                                  "Add song to playlist to start game")

    async def connect(self, ws, name, uid):
        await super().connect(ws, name, uid)
        leader = self._room.get_random_leader()
        if leader:
            await self._room.change_state(states.PlayingState, uid=leader)
            return
        players_amount = self._room.connected_players_amount()
        if players_amount == 1:
            await self._room.info(uid, "Need at least 2 players to start game")
        if players_amount == 2:
            await self._room.info(Room.BROADCAST,
                                  "Add song to playlist to start game")
        else:
            await self._room.info(uid, "Add song to playlist to start game")

    async def playlist_add(self, uid, message):
        await super().playlist_add(uid, message)
        if self._room.connected_players_amount() > 1:
            await self._room.change_state(states.PlayingState, uid=uid)
