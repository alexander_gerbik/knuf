import config
import states
from utils import AsyncTimer


class IdleState(states.BaseRoomState):
    async def enter(self):
        t = await super().enter()
        if t is not None:
            return t
        self._timer = AsyncTimer(config.IDLE_SECONDS, self._room.closed.send)
        self._timer.start()

    async def exit(self):
        self._timer.cancel()
        await super().exit()

    async def connect(self, ws, name, uid):
        await super().connect(ws, name, uid)
        await self._room.change_state(states.PendingState)
