from datetime import datetime, timezone

from models import Player, Song
from room import Room
import states


class BaseRoomState:
    message_types = [
        'message',
        'playlist_add',
        'playlist_remove',
    ]

    def __init__(self, room, **kwargs):
        if kwargs:
            self._room.logger.warning('Unexpected kwargs: %s', kwargs)
        self._room = room

    @classmethod
    async def create(cls, room, **kwargs):
        state = cls(room, **kwargs)
        t = await state.enter()
        while t is not None:
            state = t
            t = await state.enter()
        return state

    async def enter(self):
        self._room.logger.debug('Entering state %s', self.__class__.__name__)
        await self._room.send(Room.BROADCAST, await self.get_enter_message())

    async def exit(self):
        self._room.logger.debug('Exiting state %s', self.__class__.__name__)
        await self._room.send(Room.BROADCAST, {
            'type': 'state_exit',
            'state': self.__class__.__name__,
        })

    async def get_enter_message(self):
        return {
            'type': 'state_enter',
            'state': self.__class__.__name__,
        }

    async def connect(self, ws, name, uid):
        player = self._room.players.setdefault(uid, Player(name))
        player.name = name
        await self._room.send_player_info(Room.BROADCAST, uid, connected=True)
        player.ws = ws
        for p in self._room.connected_players():
            await self._room.send_player_info(uid, p, connected=True)
        await self._room.notify_playlist_changed(uid)
        await self._room.send(uid, await self.get_enter_message())
        for message in self._room._last_messages:
            await self._room.send(uid, message)
        self._room.logger.debug('%s with id %s connected', name, uid)

    async def disconnect(self, uid):
        player = self._room.players[uid]
        player.ws = None
        await self._room.send_player_info(Room.BROADCAST, uid,
                                          disconnected=True)
        self._room.logger.debug('%s disconnected', uid)
        if not any(self._room.connected_players()):
            await self._room.change_state(states.IdleState)

    async def dispatch(self, uid, message):
        message_type = message['type']
        if message_type in self.message_types:
            handler = getattr(self, message_type, self.unknown_message)
        else:
            handler = self.unknown_message

        self._room.logger.debug('got %s from %s, dispatching %s', message, uid,
                                handler.__name__)

        await handler(uid, message)

    async def unknown_message(self, uid, message):
        self._room.logger.warning(
            'State %s got unexpected message %s from user %s',
            self.__class__.__name__, message, uid)

    async def message(self, uid, message):
        message = {
            'type': 'message',
            'payload': message['payload'],
            'sender_id': uid,
            'sender': self._room.players[uid].name,
            'timestamp': datetime.now(tz=timezone.utc).isoformat(),
        }
        self._room._last_messages.append(message)
        await self._room.send(Room.BROADCAST, message)

    async def playlist_add(self, uid, message):
        name = message['name']
        artist = message['artist']
        song_id = int(message['id'])
        url = message['url']
        duration = int(message['duration'])
        begin = message.get('begin')
        end = message.get('end')
        song = Song(song_id, name, artist, duration, url, begin, end)

        player = self._room.players[uid]
        player.playlist.append(song)
        await self._room.notify_playlist_changed(uid)

    async def playlist_remove(self, uid, message):
        song_id = int(message['song_id'])
        player = self._room.players[uid]
        player.playlist.remove(song_id)
        await self._room.notify_playlist_changed(uid)
