import time

import config
from room import Room
import states
from utils import DelayableAsyncTimer


class FadeState(states.BaseRoomState):
    message_types = states.BaseRoomState.message_types + ['pay']

    def __init__(self, room, next_uid, current_uid, song, start_time):
        super().__init__(room)
        self.current_uid = current_uid
        self.next_uid = next_uid
        self.song = song
        self.start_time = start_time
        self._timer = None

    async def enter(self):
        self.deadline = min(self._have_been_played() + self._room.fade_time,
                            self.song.end)
        time_left = self.start_time + self.deadline - time.monotonic()
        self._timer = DelayableAsyncTimer(time_left, self._song_end)
        self._timer.start()

        t = await super().enter()
        if t is not None:
            return t

    async def exit(self):
        if self._timer:
            self._timer.cancel()
        await super().exit()

    async def pay(self, uid, message):
        seconds = float(message['seconds'])
        seconds = min(seconds, self.song.end - self.deadline)
        to_pay = seconds * config.PAY_MULTIPLIER
        if await self._room.withdraw(uid, to_pay):
            self._timer.advance(seconds)
            self.deadline += seconds
            await self._room.send(Room.BROADCAST, {
                'type': 'update_deadline',
                'deadline': self.deadline,
            })
        else:
            self._room.logger.warning(
                'User %s: not enough points for %s seconds', uid, seconds)

    async def get_enter_message(self):
        result = await super().get_enter_message()
        result['uid'] = self.current_uid
        result['url'] = self.song.url
        result['from'] = self._have_been_played()
        result['to'] = self.song.end
        result['current_to'] = self.deadline
        result['song'] = self.song.to_dict()
        return result

    def _have_been_played(self):
        return time.monotonic() - self.start_time

    async def _song_end(self):
        await self._room.change_state(states.PlayingState, uid=self.next_uid)
