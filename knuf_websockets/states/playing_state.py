import time

import config
from room import Room
import states
from utils import AsyncTimer


class PlayingState(states.BaseRoomState):
    message_types = states.BaseRoomState.message_types + ['guess', 'skip']

    def __init__(self, room, uid):
        super().__init__(room)
        self.losers = set()
        self.uid = uid
        self._timer = None
        self.song = None
        self.leader = self._room.players.get(self.uid)
        self.allow_skip = False

    async def enter(self):
        if self.leader:
            self.song = self.leader.playlist.pop()
            if not self.song:
                await self._room.info(self.uid, "It was your turn to propose a"
                                                " song, but you have empty "
                                                "playlist so your turn have "
                                                "been skipped. Add some song "
                                                "to your playlist.")
            await self._room.notify_playlist_changed(self.uid)

        if not self.song:
            uid = self._room.get_random_leader()
            if uid:
                return PlayingState(self._room, uid=uid)
            else:
                return states.PendingState(self._room)

        self._timer = AsyncTimer(self.song.end - self.song.begin,
                                 self._song_end)
        self._timer.start()
        self._start_time = time.monotonic() - self.song.begin

        async with self._room.db.acquire() as con:
            self._choices = await self.song.mangle(config.SONG_CHOICES, con)

        t = await super().enter()
        if t is not None:
            return t

    async def exit(self):
        points = ((self._have_been_played() - self.song.begin)
                  * config.LEADER_MULTIPLIER)
        await self._room.add_points(self.uid, points)
        if self._timer:
            self._timer.cancel()
        await super().exit()

    async def guess(self, uid, message):
        if uid in self.losers or uid == self.uid:
            self._room.logger.warning('User %s is not allowed to guess', uid)
            return
        chosen = int(message['song_id'])
        success = chosen == self.song.id
        await self._room.send(Room.BROADCAST, {
            'type': 'guess_result',
            'uid': uid,
            'song_id': chosen,
            'success': success,
        })
        if not success:
            self.losers.add(uid)
            if len(self.losers) == self._room.connected_players_amount() - 1:
                self.allow_skip = True
                await self._room.send(self.uid, {'type': 'allow_skip'})
            return
        points = self.song.end - self._have_been_played()
        await self._room.add_points(uid, points)
        await self._room.change_state(states.FadeState, next_uid=uid,
                                      current_uid=self.uid, song=self.song,
                                      start_time=self._start_time)

    async def skip(self, uid, message):
        if self.uid == uid and self.allow_skip:
            # to give right points amount to leader in exit method
            self._start_time = time.monotonic() - (self.song.end - self.song.begin)
            await self._room.change_state(PlayingState, uid=self.uid)

    async def get_enter_message(self):
        result = await super().get_enter_message()
        result['uid'] = self.uid
        result['url'] = self.song.url
        result['from'] = self._have_been_played()
        result['to'] = self.song.duration
        result['choices'] = [s.to_dict() for s in self._choices]
        result['allow_skip'] = self.allow_skip
        return result

    def _have_been_played(self):
        return time.monotonic() - self._start_time

    async def _song_end(self):
        await self._room.change_state(PlayingState, uid=self.uid)
