from django.db import models

from users.models import User


class Genre(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Artist(models.Model):
    name = models.CharField(max_length=100, unique=True)


class Song(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    genres = models.ManyToManyField(Genre)
    duration = models.PositiveSmallIntegerField()
    faved_by = models.ManyToManyField(User, through='FavouriteSong',
                                      related_name='favourite_songs')

    @property
    def full_name(self):
        return f'{self.artist.name} - {self.name}'


class FavouriteSong(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    added_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('user', 'song')


class Fragment(models.Model):
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    begin = models.FloatField()
    end = models.FloatField()
    creator = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name='fragments')
    created_at = models.DateTimeField(auto_now=True)
