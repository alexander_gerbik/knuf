import re

import bs4
import requests
from logging import getLogger

from django.conf import settings
from django.core.validators import RegexValidator

logger = getLogger('music_source.parser')

url_song_regex = r'https?://{}/Song/(?P<id>\d+)/(?P<slug>.*)'.format(
    settings.MUSIC_SOURCE)

songValidator = RegexValidator(
    url_song_regex,
    message='Url should points to song page (should have format '
            'https://{}/Song/123/blah-blah-blah)'
            .format(settings.MUSIC_SOURCE),
    code='invalid_song_url')


def get_id_from(url):
    m = re.match(url_song_regex, url)
    return m.groupdict()['id'] if m else None


def get_song_url(id):
    try:
        url = 'https://{}/Song/{}/'.format(settings.MUSIC_SOURCE, id)
        response = requests.get(url)
        response.raise_for_status()
        p = bs4.BeautifulSoup(response.text, 'lxml')
        return _get_song_url(id, p)
    except Exception:
        logger.exception('Unhandled exception while parsing song id: %s', id)
        raise


def _get_song_url(id, p):
    path = p.select('#play_{} .ico.big-song'.format(id))[0].attrs['data-url']
    return 'http://{}{}'.format(settings.MUSIC_SOURCE, path)


def retrieve_song_info(url, id):
    try:
        response = requests.get(url)
        response.raise_for_status()
        page = bs4.BeautifulSoup(response.text, 'lxml')
        info_block = get_info_block(page)
        genres = get_genres(info_block)
        artist = get_artist(info_block)
        duration = get_duration(info_block)
        name = get_song_name(page)
        return {
            'genres': genres,
            'artist': artist,
            'url': _get_song_url(id, page),
            'id': id,
            'song': {
                'duration': duration,
                'name': name,
            },
        }
    except Exception:
        logger.exception('Unhandled exception while parsing song url: %s', url)
        raise


def get_song_name(page):
    return page.select('p.big-song > span')[0].text


def get_info_block(page):
    return page.select('div.main-details > div.cont > div.tbl > table')[0]


def get_artist(info_block):
    artist = info_block.select('tr:nth-of-type(3) > td:nth-of-type(2)')[0].text
    artist = ' '.join(artist.split())
    return artist


def get_genres(info_block):
    genres = info_block.select('tr:nth-of-type(2) > td:nth-of-type(2)')[0].text
    genres = genres.split('/')
    return [g.strip() for g in genres]


def get_duration(info_block):
    dur = info_block.select('tr:nth-of-type(5) > td:nth-of-type(2)')[0].text
    mins, secs = dur.strip().split(':')
    mins, secs = int(mins), int(secs)
    return mins * 60 + secs
