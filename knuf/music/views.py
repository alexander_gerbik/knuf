from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import (
    FormView, ListView, DeleteView, CreateView, UpdateView)
from requests import HTTPError

from music.forms import AddSongForm
from music.models import Genre, Artist, Song, FavouriteSong, Fragment
from music.myzuka import retrieve_song_info, get_id_from


class ExtraContext(object):
    extra_context = {}

    def get_context_data(self, **kwargs):
        context = super(ExtraContext, self).get_context_data(**kwargs)
        context.update(self.extra_context)
        return context


class AddSongView(FormView):
    form_class = AddSongForm
    template_name = 'music/add_song.html'

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse('fav-songs')
        return reverse('index')

    def form_valid(self, form):
        url = form.cleaned_data['url']
        id = get_id_from(url)
        try:
            song = Song.objects.get(id=id)
        except Song.DoesNotExist:
            info = retrieve_song_info(url, id)
            song = create_new_song(info, id)

        user = self.request.user
        if user.is_authenticated and form.cleaned_data['add_to_favourite']:
            FavouriteSong.objects.create(user=user, song=song)

        return super().form_valid(form)


def create_new_song(info, id):
    genres = []
    for genre in info['genres']:
        genre, _ = Genre.objects.get_or_create(name=genre)
        genres.append(genre)
    artist, _ = Artist.objects.get_or_create(name=info['artist'])
    song = Song.objects.create(id=id, artist=artist, **info['song'])
    song.genres.add(*genres)
    return song


def flatten_song_info(info):
    return {
        'name': info['song']['name'],
        'artist': info['artist'],
        'id': info['id'],
        'url': info['url'],
        'duration': info['song']['duration'],
    }


def request_song(request):
    url = request.POST['url']
    id = get_id_from(url)
    if not id:
        return JsonResponse({'error': 'Wrong url'})
    try:
        info = retrieve_song_info(url, id)
    except HTTPError as e:
        if e.response.status_code == 403:
            return JsonResponse({'error': 'Banned'})
        else:
            raise
    try:
        Song.objects.get(id=id)
    except Song.DoesNotExist:
        create_new_song(info, id)
    info = flatten_song_info(info)
    return JsonResponse(info)


@login_required
def user_fragments(request):
    fragments = request.user.fragments.all()
    fragments = [{
        'name': f.song.name,
        'artist': f.song.artist.name,
        'id': f.song.id,
        'duration': f.song.duration,
        'begin': f.begin,
        'end': f.end,
    } for f in fragments]
    return JsonResponse({'fragments': fragments}, json_dumps_params={'indent': 4})


class FavouriteSongsView(LoginRequiredMixin, ListView):
    ordering = ('-added_at',)
    template_name = 'music/favourite_songs.html'
    context_object_name = 'songs'

    def get_queryset(self):
        return self.request.user.favourite_songs.prefetch_related('genres',
                                                                  'artist')


class RemoveFavSong(LoginRequiredMixin, View):
    def post(self, request, id):
        user = self.request.user
        song = get_object_or_404(Song, id=id)
        FavouriteSong.objects.filter(user=user, song=song).delete()
        return redirect('fav-songs')


class FragmentsView(LoginRequiredMixin, ListView):
    ordering = ('-created_at',)
    template_name = 'music/fragments.html'
    context_object_name = 'fragments'

    def get(self, request, *args, **kwargs):
        self.queryset = request.user.fragments.all()
        return super().get(request, *args, **kwargs)


class DeleteFragmentView(LoginRequiredMixin, DeleteView):
    model = Fragment
    success_url = reverse_lazy('fragments')
    template_name = 'music/fragment_confirm_delete.html'


class EditFragmentView(ExtraContext, LoginRequiredMixin, UpdateView):
    template_name = 'music/create_edit_fragment.html'
    extra_context = {'create': False}
    model = Fragment
    fields = ('begin', 'end')
    success_url = reverse_lazy('fragments')


class CreateFragmentView(ExtraContext, LoginRequiredMixin, CreateView):
    template_name = 'music/create_edit_fragment.html'
    extra_context = {'create': True}
    model = Fragment
    fields = ('song', 'begin', 'end')
    success_url = reverse_lazy('fragments')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.creator = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())
