from django import forms


class AddSongForm(forms.Form):
    url = forms.URLField(validators=[])
    add_to_favourite = forms.BooleanField(required=False)
