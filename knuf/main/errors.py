from functools import singledispatch

from django.contrib import messages
from django.shortcuts import redirect
from social_core.exceptions import AuthAlreadyAssociated, \
    NotAllowedToDisconnect


@singledispatch
def error_handler(exception, request):
    return None


@error_handler.register(AuthAlreadyAssociated)
def _(exception, request):
    message = exception.args[0]
    messages.add_message(request, messages.ERROR, message)
    return redirect('profile')


@error_handler.register(NotAllowedToDisconnect)
def last_social_account(exception, request):
    message = ("You can't disconnect from your last social account "
               "as you won't be able to log in in the future. "
               "You can delete your account instead.")
    messages.add_message(request, messages.ERROR, message)
    return redirect('profile')
