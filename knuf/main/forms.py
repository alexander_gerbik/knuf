from captcha.fields import ReCaptchaField
from django import forms

from main.models import Pin
from music.models import Genre


class PinForm(forms.ModelForm):
    recaptcha = ReCaptchaField()

    class Meta:
        model = Pin
        fields = ('name', 'message')


class CreateRoomForm(forms.Form):
    name = forms.CharField(max_length=255, required=True)
    # genres = forms.ModelMultipleChoiceField(Genre.objects, required=False)
    # fade_time = forms.IntegerField(min_value=0)
    private = forms.BooleanField(required=False, help_text="Only your friends can join private rooms.")
