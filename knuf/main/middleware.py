from django.conf import settings
from django.core.exceptions import MiddlewareNotUsed
from django.utils.module_loading import import_string


class ErrorDispatchMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        if not hasattr(settings, 'ERROR_HANDLER'):
            raise MiddlewareNotUsed
        handler = import_string(settings.ERROR_HANDLER)
        self._handler = handler

    def __call__(self, request):
        return self.get_response(request)

    def process_exception(self, request, exception):
        return self._handler(exception, request)
