from django import template
from django.utils.safestring import mark_safe
from django.contrib.humanize.templatetags.humanize import naturaltime

register = template.Library()


@register.filter
def field_type(bound_field):
    return bound_field.field.widget.__class__.__name__


@register.filter
def valid_class(bound_field, css_class="form-group"):
    valid_class = ''
    if bound_field.form.is_bound:
        if bound_field.errors:
            valid_class = 'has-error'
        elif field_type(bound_field) != 'PasswordInput':
            valid_class = 'has-success'
    return f'{css_class} {valid_class}'


@register.filter
def titled_naturaltime(date, format=None):
    if format is None:
        exact = date.isoformat()
    else:
        exact = date.strftime(format)
    natural = naturaltime(date)

    return mark_safe(
        f'<span title="{exact}">{natural}</span>')
