import jwt
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, CreateView, FormView

from main.forms import PinForm, CreateRoomForm
from main.models import Pin
from main.ws_server_api import list_rooms as ws_list_rooms, create_room
from music.models import Genre
from users.models import User


def index(request):
    return render(request, 'index.html')


def tic_tac_toe(request):
    return render(request, 'tic-tac-toe.html')


class PinView(ListView, CreateView):
    model = Pin
    ordering = ('-creation_date',)
    paginate_by = 50
    template_name = 'main/pins.html'

    form_class = PinForm
    success_url = reverse_lazy('pins')

    def get_context_object_name(self, object_list):
        return None

    def get_initial(self):
        initial = super().get_initial()
        user = self.request.user
        if user.is_authenticated:
            initial['name'] = user.username
        return initial

    def get(self, request, *args, **kwargs):
        self.object = None
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        if (self.request.method == 'GET' or
                ('form' in kwargs and not kwargs['form'].is_valid())):
            if not hasattr(self, 'object_list'):
                self.object_list = self.get_queryset()
            return super().get_context_data(**kwargs)
        return super(ListView, self).get_context_data(**kwargs)


class CreateRoomView(LoginRequiredMixin, FormView):
    form_class = CreateRoomForm
    template_name = 'main/create_room.html'
    initial = {'fade_time': 15}

    def get_success_url(self):
        return reverse('join-room', args=(self.id,))

    def form_valid(self, form):
        name = form.cleaned_data['name']
        # genres = form.cleaned_data['genres']
        # fade_time = form.cleaned_data['fade_time']
        genres = []
        fade_time = self.initial['fade_time']
        private = form.cleaned_data['private']
        self.id = create_room(self.request, self.request.user, name, genres,
                              fade_time, private)
        return super().form_valid(form)


def list_rooms(request):
    rooms = ws_list_rooms(request)
    public_rooms = [room for room in rooms if not room['private']]
    private_rooms = allowed_private_rooms(rooms, request.user)
    rooms = private_rooms + public_rooms
    for room in rooms:
        room['creator'] = User.objects.get(id=room['creator'])
        room['genres'] = [Genre.objects.get(id=id) for id in room['genres']]
    return render(request, 'main/rooms_list.html', {'rooms': rooms})


def allowed_private_rooms(rooms, user):
    if not user.is_authenticated:
        return []
    friends = set(f.id for f in user.friends.all())
    friends.add(user.id)
    rooms = [r for r in rooms if r['creator'] in friends and r['private']]
    return rooms


def add_jwt_token(request, response):
    user = request.user
    payload = {'username': user.username, 'id': user.id}
    token = jwt.encode(payload, settings.SECRET_KEY).decode()
    response.set_cookie('jwt_token', token)


@login_required
def join_room(request, id):
    response = render(request, 'main/room.html', {
        'id': id,
        'pay_multiplier': settings.PAY_MULTIPLIER,
    })
    add_jwt_token(request, response)
    return response
