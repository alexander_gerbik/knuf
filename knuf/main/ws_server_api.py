import requests
from django.conf import settings


def list_rooms(request):
    """
    :return: list of dictionaries. Each dictionary has following items:
    id - room id (int)
    creator - creator id (int)
    name - room name (str)
    genres - list of genre ids (List[int])
    fade_time - (int)
    private - (bool)
    """
    uri = request.build_absolute_uri('/rooms/')
    response = requests.get(uri)
    return response.json()['rooms']


def create_room(request, creator, name, genres, fade_time, private):
    """
    :param creator: User model
    :param name:  str
    :param genres: iterator of Geanre models
    :param fade_time: int
    :param private: bool
    :return: int - id of created room
    """
    data = {
        'creator': creator.id,
        'name': name,
        'genres': [genre.id for genre in genres],
        'fade_time': fade_time,
        'private': private
    }
    uri = request.build_absolute_uri('/create/')
    response = requests.post(uri, data=data)
    return response.json()['id']
