import os
from django.test import TestCase
from django.urls import resolve

from main.models import Pin
from main.views import PinView
from users.tests.utils import get_user_and_client


class PinViewTestCase(TestCase):
    url = '/pins/'

    def setUp(self):
        os.environ['RECAPTCHA_TESTING'] = 'True'

    def tearDown(self):
        del os.environ['RECAPTCHA_TESTING']

    def test_view__ok(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

    def test_authenticated__pre_filled_name(self):
        ben, client = get_user_and_client('ben')

        response = client.get(self.url)

        self.assertEqual(response.context['form'].initial['name'], 'ben')

    def test_valid_form__pin_created(self):
        expected = {
            'name': 'ben',
            'message': 'Hello, world!',
        }
        data = {'g-recaptcha-response': 'PASSED'}
        data.update(expected)

        response = self.client.post(self.url, data=data)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, self.url)
        pin = Pin.objects.values('name', 'message').first()
        self.assertEqual(pin, expected)

    def test_invalid_form__status_code(self):
        data = {
            'name': 'ben',
            'message': 'Hello, world!',
            'g-recaptcha-response': 'PASSED',
        }

        response = self.client.post(self.url, data=data)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, self.url)

    def test_resolves(self):
        view = resolve(self.url)
        self.assertEqual(view.func.view_class, PinView)
