from django.conf.urls import url
from .views import (index, PinView, CreateRoomView, list_rooms, join_room,
                    tic_tac_toe)
from music.views import (
    AddSongView, FavouriteSongsView, FragmentsView, RemoveFavSong,
    request_song, user_fragments, DeleteFragmentView, CreateFragmentView, EditFragmentView)

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^tic-tac-toe/$', tic_tac_toe, name='tic-tac-toe'),
    url(r'^pins/$', PinView.as_view(), name='pins'),
    url(r'^add-song/$', AddSongView.as_view(), name='add-song'),
    url(r'^fav-songs/$', FavouriteSongsView.as_view(), name='fav-songs'),
    url(r'^remove-fav-song/(\d+)/$', RemoveFavSong.as_view(),
        name='remove-fav-song'),
    url(r'^fragments/$', FragmentsView.as_view(), name='fragments'),
    url(r'^fragments/(?P<pk>\d+)/remove/$', DeleteFragmentView.as_view(),
        name='remove-fragment'),
    url(r'^fragments/create/$', CreateFragmentView.as_view(),
        name='create-fragment'),
    url(r'^fragments/(?P<pk>\d+)/edit/$', EditFragmentView.as_view(),
        name='edit-fragment'),
    url(r'^js/request-song/$', request_song, name='request-song'),
    url(r'^js/user-fragments/$', user_fragments, name='user-fragments'),
    url(r'^room/create/$', CreateRoomView.as_view(), name='create-room'),
    url(r'^room/list/$', list_rooms, name='list-rooms'),
    url(r'^room/(\d+)/$', join_room, name='join-room'),
]
