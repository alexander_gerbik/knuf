from django.db import models


class Pin(models.Model):
    name = models.CharField(max_length=50, blank=True)
    message = models.TextField()
    creation_date = models.DateTimeField(auto_now=True)
