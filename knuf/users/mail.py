from django.conf import settings
from django.core.mail import send_mail
from django.urls import reverse


def get_validation_url(strategy, code):
    url = reverse('token_login', args=(code.code,))
    url = strategy.request.build_absolute_uri(url)
    return url


def send_validation(strategy, backend, code, partial_token):
    url = get_validation_url(strategy, code)
    subject = '{0} sign up/log in'.format(strategy.request.get_host())
    message = 'Use this url to login {0}'.format(url)
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [code.email])
