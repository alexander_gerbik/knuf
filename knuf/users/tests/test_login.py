from django.contrib import auth
from django.core import mail
from django.test import TestCase, Client
from django.urls import resolve, reverse
from unittest.mock import patch

from urllib.parse import urlparse

from users.mail import send_validation, get_validation_url
from users.views import login_form


class PasswordlessLoginTestCase(TestCase):
    url = '/users/login-form/'
    email = 'bob@gmail.com'

    def test_not_authenticated__ok(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

    def test_resolves(self):
        view = resolve(self.url)
        self.assertEqual(view.func, login_form)

    def test_form_submit__mail_sent(self):
        response = self.client.post(
            reverse('social:complete', args=('email',)),
            data={'email': self.email})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/users/validation-sent/')

        self.assertEqual(len(mail.outbox), 1)

    def test_redirects(self):
        response = self.client.get('/users/validation-sent/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/users/login-form/')

    def send_validation(self, strategy, backend, code, partial_token):
        validation_url = get_validation_url(strategy, code)
        self.validation_url = urlparse(validation_url).path
        return send_validation(strategy, backend, code, partial_token)

    def test_login_process(self):
        with patch('users.mail.send_validation', self.send_validation):
            self.client.post(
                reverse('social:complete', args=('email',)),
                data={'email': self.email})
            user = auth.get_user(self.client)
            self.assertFalse(user.is_authenticated())

            self.client.get(self.validation_url, follow=True)
            user = auth.get_user(self.client)
            self.assertTrue(user.is_authenticated())

    def sign_up(self):
        client = Client()
        with patch('users.mail.send_validation', self.send_validation):
            client.post(
                reverse('social:complete', args=('email',)),
                data={'email': self.email})
            client.get(self.validation_url, follow=True)

    def test_send_validation__not_authenticated(self):
        """
            If SOCIAL_AUTH_EMAIL_PASSWORDLESS=False any user can login as
            existing user just entering his email and not following validation
            link. This test prevents this behaviour as it's high security risk.
        """
        self.sign_up()
        with patch('users.mail.send_validation', self.send_validation):
            self.client.post(
                reverse('social:complete', args=('email',)),
                data={'email': self.email})
            user = auth.get_user(self.client)
            self.assertFalse(user.is_authenticated())
