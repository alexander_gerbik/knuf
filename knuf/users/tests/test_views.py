from django.test import TestCase
from django.urls import resolve

from users.tests.utils import get_user_and_client
from users.views import (
    FriendshipRequestsView, FriendsView, IgnoreFriendView,
    RemoveFriendView, AddFriendView, EditProfileView, DeleteAccountView,
    UserView, profile, login)


class ViewTestCase(TestCase):
    def setUp(self):
        self.bob, self.bob_client = get_user_and_client("bob")


class FriendshipRequestsViewTestCase(ViewTestCase):
    url = '/users/friendship-requests/'

    def test_authenticated__ok(self):
        response = self.bob_client.get(self.url)

        self.assertEqual(response.status_code, 200)

    def test_not_authenticated__redirect_to_login(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 302)

    def test_resolves(self):
        view = resolve(self.url)
        self.assertEqual(view.func.view_class, FriendshipRequestsView)


class FriendsRequestsViewTestCase(ViewTestCase):
    url = '/users/friends/'

    def test_authenticated__ok(self):
        response = self.bob_client.get(self.url)

        self.assertEqual(response.status_code, 200)

    def test_not_authenticated__redirect_to_login(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 302)

    def test_resolves(self):
        view = resolve(self.url)
        self.assertEqual(view.func.view_class, FriendsView)


class IgnoreFriendViewTestCase(ViewTestCase):
    url = '/users/ignore-friend/{}/'

    def setUp(self):
        super().setUp()
        self.alice, _ = get_user_and_client('alice')

    def test_not_authenticated__redirect_to_login(self):
        response = self.client.get(self.url.format(self.alice.id))

        self.assertEqual(response.status_code, 302)

    def test_resolves(self):
        view = resolve(self.url.format(self.alice.id))
        self.assertEqual(view.func.view_class, IgnoreFriendView)


class RemoveFriendViewTestCase(ViewTestCase):
    url = '/users/remove-friend/{}/'

    def setUp(self):
        super().setUp()
        self.alice, _ = get_user_and_client('alice')

    def test_not_authenticated__redirect_to_login(self):
        response = self.client.get(self.url.format(self.alice.id))

        self.assertEqual(response.status_code, 302)

    def test_non_existent_id__404(self):
        non_existent_id = 322
        response = self.bob_client.post(self.url.format(non_existent_id))

        self.assertEqual(response.status_code, 404)

    def test_resolves(self):
        view = resolve(self.url.format(self.alice.id))
        self.assertEqual(view.func.view_class, RemoveFriendView)


class AddFriendViewTestCase(ViewTestCase):
    url = '/users/add-friend/{}/'

    def setUp(self):
        super().setUp()
        self.alice, _ = get_user_and_client('alice')

    def test_not_authenticated__redirect_to_login(self):
        response = self.client.get(self.url.format(self.alice.id))

        self.assertEqual(response.status_code, 302)

    def test_non_existent_id__404(self):
        non_existent_id = 322
        response = self.bob_client.post(self.url.format(non_existent_id))

        self.assertEqual(response.status_code, 404)

    def test_resolves(self):
        view = resolve(self.url.format(self.alice.id))
        self.assertEqual(view.func.view_class, AddFriendView)


class DeleteAccountViewTestCase(ViewTestCase):
    url = '/users/delete-account/'

    def test_authenticated__ok(self):
        response = self.bob_client.get(self.url)

        self.assertEqual(response.status_code, 200)

    def test_not_authenticated__redirect_to_login(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 302)

    def test_resolves(self):
        view = resolve(self.url)
        self.assertEqual(view.func.view_class, DeleteAccountView)


class EditProfileViewTestCase(ViewTestCase):
    url = '/users/edit-profile/'

    def test_authenticated__ok(self):
        response = self.bob_client.get(self.url)

        self.assertEqual(response.status_code, 200)

    def test_not_authenticated__redirect_to_login(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 302)

    def test_resolves(self):
        view = resolve(self.url)
        self.assertEqual(view.func.view_class, EditProfileView)


class UserViewViewTestCase(ViewTestCase):
    url = '/users/user/{}/'

    def setUp(self):
        super().setUp()
        self.alice, _ = get_user_and_client('alice')

    def test_authenticated__ok(self):
        response = self.bob_client.get(self.url.format(self.alice))

        self.assertEqual(response.status_code, 200)

    def test_not_authenticated__ok(self):
        response = self.client.get(self.url.format(self.alice))

        self.assertEqual(response.status_code, 200)

    def test_resolves(self):
        view = resolve(self.url.format(self.alice))
        self.assertEqual(view.func.view_class, UserView)


class ProfileViewTestCase(ViewTestCase):
    url = '/users/profile/'

    def test_authenticated__redirect(self):
        response = self.bob_client.get(self.url)

        self.assertEqual(response.status_code, 302)
        self.assertEqual('/users/user/bob/', response.url)

    def test_not_authenticated__redirect_to_login(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 302)
        self.assertEqual('/users/login/?next=/users/profile/', response.url)

    def test_resolves(self):
        view = resolve(self.url)
        self.assertEqual(view.func, profile)


class LoginViewTestCase(ViewTestCase):
    url = '/users/login/'

    def test_not_authenticated__ok(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

    def test_resolves(self):
        view = resolve(self.url)
        self.assertEqual(view.func, login)
