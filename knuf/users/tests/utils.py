from django.test import Client

from users.models import User


def get_user_and_client(username):
    email = '{}@gmail.com'.format(username)
    user = User.objects.create_user(username=username, email=email)
    client = Client()
    client.force_login(user)
    return user, client
