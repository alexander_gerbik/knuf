from django.test import TestCase
from django.urls import reverse

from users.tests.utils import get_user_and_client


class TestFriends(TestCase):
    def setUp(self):
        self.ben, self.ben_client = get_user_and_client('ben')
        self.bob, self.bob_client = get_user_and_client('bob')

    def test_friendship_request(self):
        self.assertFalse(self.bob.has_incoming_friendship_requests())

        response = self.ben_client.post(
            reverse('add_friend', args=(self.bob.id,)))

        self.assertEqual(response.status_code, 302)

        self.assertIn(self.ben, self.bob.followers())
        self.assertIn(self.bob, self.ben.following())
        self.assertNotIn(self.ben, self.bob.friends.all())
        self.assertNotIn(self.bob, self.ben.friends.all())

    def test_friendship(self):
        self.ben_client.post(reverse('add_friend', args=(self.bob.id,)))
        self.bob_client.post(reverse('add_friend', args=(self.ben.id,)))

        self.assertIn(self.ben, self.bob.friends.all())
        self.assertIn(self.bob, self.ben.friends.all())
        self.assertNotIn(self.ben, self.bob.followers())
        self.assertNotIn(self.bob, self.ben.following())
        self.assertFalse(self.bob.has_incoming_friendship_requests())

    def test_friend_remove(self):
        self.ben.friends.add(self.bob)

        response = self.ben_client.post(
            reverse('remove_friend', args=(self.bob.id,)))

        self.assertEqual(response.status_code, 302)

        self.assertIn(self.bob, self.ben.ignored_followers())
        self.assertIn(self.ben, self.bob.following())
        self.assertNotIn(self.ben, self.bob.friends.all())
        self.assertNotIn(self.bob, self.ben.friends.all())
        self.assertFalse(self.ben.has_incoming_friendship_requests())

        self.bob_client.post(reverse('remove_friend', args=(self.ben.id,)))
        self.assertNotIn(self.ben, self.bob.following())
        self.assertNotIn(self.bob, self.ben.following())
        self.assertNotIn(self.ben, self.bob.friends.all())

    def test_cant_add_yourself_to_friends(self):
        bob = self.bob

        bob.add_friend(bob)

        self.assertNotIn(bob, bob.friends.all())


class TestFriendshipRequestsView(TestCase):
    def setUp(self):
        self.ben, self.ben_client = get_user_and_client('ben')
        self.bob, self.bob_client = get_user_and_client('bob')
        # self.alice, self.alice_client = get_user_and_client('alice')
        # self.mallory, self.mallory_client = get_user_and_client('mallory')

    def test_followers_shown_in_friendship_requests(self):
        self.bob.add_friend(self.ben)

        response = self.ben_client.get(reverse('friendship_requests'))

        self.assertEqual(response.status_code, 200)
        self.assertIn(self.bob, response.context['followers'])

    def test_ignored_followers_shown_in_friendship_requests(self):
        self.bob.add_friend(self.ben)
        response = self.ben_client.post(
            reverse('ignore_friend', args=(self.bob.id,)))

        self.assertEqual(response.status_code, 302)

        response = self.ben_client.get(reverse('friendship_requests'))

        self.assertEqual(response.status_code, 200)
        self.assertIn(self.bob, response.context['ignored_followers'])

    def test_following_shown_in_friendship_requests(self):
        self.bob.add_friend(self.ben)

        response = self.bob_client.get(reverse('friendship_requests'))

        self.assertEqual(response.status_code, 200)
        self.assertIn(self.ben, response.context['following'])
