from django.test import TestCase

from users.models import User
from users.templatetags.gravatar import gravatar


class TestGravatar(TestCase):
    def test_gravatar(self):
        email = 'MyEmailAddress@example.com  '
        user = User.objects.create_user(email=email, username='ben')
        size = 200
        expected = ('https://www.gravatar.com/avatar/'
                    '0bc83cb571cd1c50ba6f3e8a78ef1346?d=identicon&s=200')
        actual = gravatar(user, size)
        self.assertEqual(actual, expected)
