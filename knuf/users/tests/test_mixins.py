from django.contrib.auth.views import LoginView
from django.core.exceptions import ImproperlyConfigured
from django.test import TestCase, RequestFactory

from users.mixins import RedirectToSuccessUrlMixin, RedirectToPreviousMixin


class SuccessUrlInitialized(RedirectToSuccessUrlMixin):
    success_url = '/qwe/'


class SuccessUrlNotInitialized(RedirectToSuccessUrlMixin):
    pass


class RedirectToSuccessUrlMixinTestCase(TestCase):
    def test_success_url_initialized_redirects_to_it(self):
        c = SuccessUrlInitialized()
        url = c.get_success_url()
        self.assertEqual(url, '/qwe/')

    def test_success_url_not_initialized_throws(self):
        c = SuccessUrlNotInitialized()
        with self.assertRaises(ImproperlyConfigured):
            c.get_success_url()


class RefererRedirectLogin(RedirectToPreviousMixin, LoginView):
    pass


class RedirectRefererTestCase(TestCase):
    def test_http_referer_exists__redirects_to_it(self):
        previous = '/previous/'
        mixin = RedirectToPreviousMixin()
        mixin.request = RequestFactory().post('/login/', HTTP_REFERER=previous)

        url = mixin.get_success_url()

        self.assertEqual(previous, url)

    def test_http_referer_doesnt_exist__calls_super(self):
        success_url = '/not/previous/'
        view = RefererRedirectLogin()
        view.request = RequestFactory().post('/login/')
        view.request.POST = {'next': success_url}

        url = view.get_success_url()

        self.assertEqual(success_url, url)

    def test_no_get_success_url__raises(self):
        mixin = RedirectToPreviousMixin()
        mixin.request = RequestFactory().post('/login/')

        with self.assertRaises(ImproperlyConfigured):
            mixin.get_success_url()
