from django.core.exceptions import ImproperlyConfigured
from django.utils.encoding import force_text
from django.utils.http import is_safe_url


class RedirectToSuccessUrlMixin:
    def get_success_url(self):
        """
        Returns the supplied success URL.
        """
        if hasattr(self, 'success_url') and self.success_url:
            # Forcing possible reverse_lazy evaluation
            url = force_text(self.success_url)
        else:
            raise ImproperlyConfigured(
                "No URL to redirect to. Provide a success_url.")
        return url


class RedirectToPreviousMixin:
    def get_success_url(self):
        url = self.request.META.get('HTTP_REFERER')
        if hasattr(self, 'get_success_url_allowed_hosts'):
            allowed_hosts = self.get_success_url_allowed_hosts()
        else:
            allowed_hosts = [self.request.get_host()]
        if url and is_safe_url(url=url, allowed_hosts=allowed_hosts,
                               require_https=self.request.is_secure()):
            return url
        try:
            return super().get_success_url()
        except AttributeError:
            raise ImproperlyConfigured(
                "No URL to redirect to. HTTP_REFERER is not set "
                "and parent has no get_success_url method")


class RefererOrSuccessMixin(RedirectToPreviousMixin,
                            RedirectToSuccessUrlMixin):
    pass
