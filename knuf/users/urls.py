from django.conf.urls import url, include
from . import views

from django.contrib.auth import views as auth_views

urlpatterns = [
    url('', include('social_django.urls', namespace='social')),
    url(r'^login/$', views.login, name='login'),
    url(r'^login-form/$', views.login_form, name='login_form'),
    url(r'^telegram-login/$', views.telegram_login, name='telegram_login'),
    url(r'^validation-sent/$', views.validation_sent, name='validation_sent'),
    url(r'^token-login/(?P<token>[^/]+)/$', views.token_login,
        name='token_login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^user/(?P<username>[\w.@+-]+)/$', views.UserView.as_view(),
        name='user'),
    url(r'^edit-profile/$', views.EditProfileView.as_view(),
        name='edit_profile'),
    url(r'^delete-account/$', views.DeleteAccountView.as_view(),
        name='delete_account'),
    url(r'^add-friend/(?P<user_id>\d+)/$', views.AddFriendView.as_view(),
        name='add_friend'),
    url(r'^remove-friend/(?P<user_id>\d+)/$', views.RemoveFriendView.as_view(),
        name='remove_friend'),
    url(r'^ignore-friend/(?P<user_id>\d+)/$',
        views.IgnoreFriendView.as_view(), name='ignore_friend'),
    url(r'^friends/$', views.FriendsView.as_view(),
        name='friends'),
    url(r'^friendship-requests/$', views.FriendshipRequestsView.as_view(),
        name='friendship_requests'),
]
