from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import UpdateView, DetailView, DeleteView, ListView

from users.mixins import RefererOrSuccessMixin
from users.models import FriendshipRequest


def login_form(request):
    return render(request, 'users/passwordless_login.html')


def telegram_login(request):
    complete_url = reverse('social:complete', kwargs={'backend': 'telegram'})
    complete_url = request.build_absolute_uri(complete_url)
    bot_name = settings.SOCIAL_AUTH_TELEGRAM_BOT_NAME
    return render(request, 'users/telegram_login.html', {
        'complete_url': complete_url,
        'bot_name': bot_name,
    })


def validation_sent(request):
    if 'email_validation_address' not in request.session:
        return redirect('login_form')
    email = request.session['email_validation_address']
    message = 'We sent you a temporary login token to {}'.format(email)
    messages.add_message(request, messages.SUCCESS, message)
    return redirect('index')


def token_login(request, token):
    url = reverse('social:complete', args=('email',))
    url += '?verification_code={}'.format(token)
    return redirect(url)


def login(request):
    return render(request, 'users/login.html')


@login_required
def profile(request):
    return redirect('user', username=request.user.username)


class UserView(DetailView):
    template_name = 'users/user_detail.html'

    def get_object(self, queryset=None):
        return get_object_or_404(get_user_model(),
                                 username=self.kwargs['username'])


class EditProfileView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    fields = ('username',)
    template_name = 'users/edit_profile.html'
    success_url = reverse_lazy('profile')

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS,
                             'profile has changed successfully')
        return super().form_valid(form)


class DeleteAccountView(LoginRequiredMixin, DeleteView):
    template_name = 'users/user_confirm_delete.html'

    success_url = reverse_lazy('index')

    def get_object(self, queryset=None):
        return self.request.user


class AddFriendView(RefererOrSuccessMixin, LoginRequiredMixin, View):
    success_url = reverse_lazy('profile')

    def post(self, request, user_id):
        target = get_object_or_404(get_user_model(), id=user_id)
        user = self.request.user
        friends = user.add_friend(target)
        if not friends:
            messages.add_message(request, messages.SUCCESS,
                                 'Request for friendship has been sent')
        return redirect(self.get_success_url())


class RemoveFriendView(RefererOrSuccessMixin, LoginRequiredMixin, View):
    success_url = reverse_lazy('profile')

    def post(self, request, user_id):
        target = get_object_or_404(get_user_model(), id=user_id)
        user = self.request.user
        user.remove_friend(target)
        return redirect(self.get_success_url())


class IgnoreFriendView(RefererOrSuccessMixin, LoginRequiredMixin, View):
    success_url = reverse_lazy('profile')

    def post(self, request, user_id):
        FriendshipRequest.objects.filter(initiator_id=user_id).update(
            ignored=True)
        return redirect(self.get_success_url())


class FriendsView(LoginRequiredMixin, ListView):
    context_object_name = 'friends'
    paginate_by = 10
    template_name = 'users/friends.html'

    def get_queryset(self):
        return self.request.user.friends.order_by('username').all()


class FriendshipRequestsView(LoginRequiredMixin, View):
    def get(self, request):
        user = self.request.user
        context = {}
        context['followers'] = user.followers()
        context['ignored_followers'] = user.ignored_followers()
        context['following'] = user.following()
        return render(request, 'users/friendship-requests.html', context)
