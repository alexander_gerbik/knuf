from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    friends = models.ManyToManyField('self', symmetrical=True)

    def add_friend(self, other):
        """
        :param other: User
            Another user.
        :return: Bool
            True if you 'self' and 'other' are now friends (that's means there
            was an incoming friendship request from user 'other'.
            False otherwise.
        """
        if other == self:
            return False
        try:
            counteroffer = FriendshipRequest.objects.get(
                initiator=other, target=self)
        except FriendshipRequest.DoesNotExist:
            FriendshipRequest.objects.create(initiator=self, target=other)
            return False
        self.friends.add(other)
        counteroffer.delete()
        return True

    def remove_friend(self, other):
        if other in self.friends.all():
            self.friends.remove(other)
            FriendshipRequest.objects.create(initiator=other, target=self,
                                             ignored=True)
            return
        FriendshipRequest.objects.filter(initiator=self, target=other).delete()

    def has_incoming_friendship_requests(self):
        return FriendshipRequest.objects.filter(target=self,
                                                ignored=False).exists()

    def _followers(self):
        return User.objects.filter(outgoing_friendship__target=self)

    def followers(self):
        return self._followers().filter(outgoing_friendship__ignored=False)

    def ignored_followers(self):
        return self._followers().filter(outgoing_friendship__ignored=True)

    def following(self):
        return User.objects.filter(incoming_friendship__initiator=self)


class FriendshipRequest(models.Model):
    initiator = models.ForeignKey(User, models.CASCADE,
                                  'outgoing_friendship')
    target = models.ForeignKey(User, models.CASCADE,
                               'incoming_friendship')
    ignored = models.BooleanField(default=False)

    class Meta:
        unique_together = ('initiator', 'target')
