from django import template
from django.template.loader import render_to_string

register = template.Library()


@register.simple_tag(takes_context=True)
def login(context, backend):
    req = context['request']
    return render_with_fallback(backend, req, 'users/auth_backends/login')


@register.simple_tag(takes_context=True)
def connect(context, backend):
    req = context['request']
    return render_with_fallback(backend, req, 'users/auth_backends/connect')


@register.simple_tag(takes_context=True)
def disconnect(context, backend):
    req = context['request']
    return render_with_fallback(backend, req, 'users/auth_backends/disconnect')


def render_with_fallback(backend, request, directory):
    templates = [
        '{}/{}.html'.format(directory, backend),
        '{}/default.html'.format(directory),
    ]
    return render_to_string(templates, {
        'backend': backend,
    }, request)
