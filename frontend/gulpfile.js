const autoprefixer = require('gulp-autoprefixer');
const babelify = require('babelify');
const browserify = require('browserify');
const buffer = require('vinyl-buffer');
const csso = require('gulp-csso');
const eslint = require('gulp-eslint');
const gulp = require('gulp');
const sass = require('gulp-sass');
const source = require('vinyl-source-stream');
const sourcemaps = require('gulp-sourcemaps');
const streamify = require('gulp-streamify');
const uglify = require('gulp-uglify');


const entryPoint = 'src/js/index.js';
const jsSources = 'src/**/*.js{,x}';
const htmlSources = 'src/**/*.html';
const sassSources = 'src/**/*.sass';
const buildDir = '../knuf/main/static/';

gulp.task('js', () => {
    return browserify(entryPoint, {
        debug: true,
        extensions: ['.jsx'],
    })
        .transform(babelify, {
            presets: ["env", "react"],
            sourceMaps: true,
        })
        .bundle()
        .pipe(source('js/bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
            // .pipe(streamify(uglify()))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(buildDir));
});

gulp.task('sass', function () {
    return gulp.src(sassSources)
        .pipe(sourcemaps.init())
            .pipe(sass())
            .pipe(autoprefixer())
            // .pipe(csso())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(buildDir));
});

gulp.task('html', () => {
    return gulp.src(htmlSources)
        .pipe(gulp.dest(buildDir));
});

gulp.task('lint', () => {
    return gulp.src(jsSources)
        .pipe(eslint('eslintrc.json'))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('social', function () {
    return gulp.src('node_modules/bootstrap-social/bootstrap-social.css',
        {base: './node_modules/bootstrap-social/'})
    .pipe(gulp.dest(buildDir + '/css'));
});

gulp.task('bootstrap-notify', function () {
    return gulp.src('node_modules/bootstrap-notify/bootstrap-notify.js',
        {base: './node_modules/bootstrap-notify/'})
    .pipe(gulp.dest(buildDir + '/js'));
});

gulp.task('watch', () => {

    gulp.watch(jsSources, ['js']);
    gulp.watch(sassSources, ['sass']);
    gulp.watch(htmlSources, ['html']);
});

gulp.task('default',
    [
        'lint',
        'html',
        'js',
        'sass',
        'social',
        'bootstrap-notify',
    ]);
