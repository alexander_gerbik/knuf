import React from "react";

class TwoHandleRange extends React.Component {
    constructor(props) {
        super(props);
        this.range = React.createRef();
    }

    componentDidMount() {
        let self = this;
        $(this.range.current).slider({
            range: true,
            min: 0,
            max: this.props.max,
            step: 0.01,
            values: [this.props.left, this.props.right],
            slide: function (evenut, ui) {
                self.props.onChange(ui.values[0], ui.values[1]);
            },
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        $(this.range.current).slider("values", 0, this.props.left);
        $(this.range.current).slider("values", 1, this.props.right);
    }

    render() {
        return (
            <div className={this.props.className} ref={this.range}></div>
        );
    }
}

export default TwoHandleRange;
