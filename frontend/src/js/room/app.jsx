import React from "react";
import UserList from "./chat/userList";
import MessageList from "./chat/messageList";
import AddMessage from "./chat/addMessage";
import Playlist from "./playlist";
import ChoiceSelector from "./choiceSelector";
import FadeController from "./FadeController";
import AudioComponent from "./AudioComponent";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            users: [],
            playlist: [],
            choices: [],
            nowPlaying: '',
            songUrl: '',
            from: null,
            to: null,
            maxTo: null,
            allowSkip: false,
        };
        this.dispatcher = props.dispatcher;

        this.dispatcher.on('message', this.handleIncomingMessage.bind(this));
        this.dispatcher.on('update_user', this.handleUserUpdate.bind(this));
        this.dispatcher.on('update_playlist', this.handlePlaylistUpdate.bind(this));
        this.dispatcher.on('state_enter', this.stateEnter.bind(this));
        this.dispatcher.on('state_exit', this.stateExit.bind(this));
        this.dispatcher.on('guess_result', this.handleGuess.bind(this));
        this.dispatcher.on('update_deadline', this.handleUpdateDeadline.bind(this));
        this.dispatcher.on('allow_skip', this.handleAllowSkip.bind(this));
    }

    handleGuess(message) {
        let uid = message.uid;
        let success = message.success;
        let songId = message.song_id;

        let choices = this.state.choices;
        if (uid == this.props.id) {
            choices = this.state.choices.map((choice) => Object.assign({}, choice, {
                state: choice.id !== songId ? "disabled" : message.success ? "right" : "wrong",
            }));
        }
        let users = this.state.users.map((user) => Object.assign({}, user, {
            state: user.uid !== uid ? user.state : message.success ? "right" : "wrong",
        }));

        this.setState({
            choices: choices,
            users: users,
        });
    }

    stateEnter(message) {
        let state = message.state;
        delete message.state;

        if (state === "PlayingState") {
            this.beginPlay(message);
        } else if (state === "FadeState") {
            this.setState({
                nowPlaying: message.song.artist + ' - ' + message.song.name,
                songUrl: message.url,
                from: message.from,
                to: message.current_to,
                maxTo: message.to,
            });
        } else if (state === "PendingState") {
            this.setState({
                nowPlaying: '',
                songUrl: '',
                from: null,
                to: null,
                maxTo: null,
            });
        }
    }

    stateExit(message) {
        let state = message.state;
        delete message.state;

        if (state === "PlayingState") {
            this.setState({
                choices: [],
            });
        } else if (state === "FadeState") {
            this.setState({
                nowPlaying: '',
            });
        }
    }

    beginPlay(message) {
        const leaderId = message.uid;

        let users = this.state.users.map((user) => Object.assign({}, user, {
            state: user.uid === leaderId ? "leader" : "passive",
        }));

        if (this.props.id === leaderId) {
            message.choices.forEach((choice) => choice.state = "disabled");
        } else {
            message.choices.forEach((choice) => choice.state = "enabled");
        }
        this.setState({
            users: users,
            choices: message.choices,
            songUrl: message.url,
            from: message.from,
            to: message.to,
            maxTo: null,
            allowSkip: message.allow_skip && message.uid == this.props.id,
        });
    }

    handleAllowSkip(message) {
        this.setState({
            allowSkip: true,
        });
    }

    handleUpdateDeadline(message) {
        this.setState({
            to: message.deadline,
        });
    }

    handlePlaylistUpdate(message) {
        this.setState({
            playlist: message.playlist,
        });
    }

    handleIncomingMessage(message) {
        this.setState({
            messages: this.state.messages.concat([message]),
        });
    }

    handleUserUpdate(user) {
        const uid = user.uid;
        const disconnected = user.disconnected;

        if (user.connected) {
            user.state = "passive";
        } else {
            let prev = this.state.users.find(user => user.uid === uid);
            user.state = prev.state;
        }

        delete user.connected;
        delete user.disconnected;

        let newUsers = this.state.users.filter(user => user.uid !== uid);
        if (!disconnected) {
            newUsers = newUsers.concat([user]);
        }
        newUsers.sort((a, b) => b.score - a.score);
        this.setState({
            users: newUsers,
        });
    }

    removeSong(id) {
        this.dispatcher.send({"song_id": id}, 'playlist_remove');
    }

    guess(id) {
        this.dispatcher.send({'song_id': id}, 'guess');
    }

    extend(seconds) {
        this.dispatcher.send({'seconds': seconds}, 'pay');
    }

    getPlayerPoints() {
        let user = this.state.users.find(user => user.uid === this.props.id);
        if (user === undefined) {
            return 0;
        }
        return user.points;
    }

    render() {
        let points = this.getPlayerPoints();
        return (
            <div className="row">
                <div className="col-lg-5">
                    {this.state.songUrl !== '' &&
                    <AudioComponent songUrl={this.state.songUrl}
                                    from={this.state.from}
                                    to={this.state.to}
                                    maxTo={this.state.maxTo}
                                    allowSkip={this.state.allowSkip}
                                    onSkip={() => this.dispatcher.send({}, 'skip')}/>}
                    {this.state.choices.length > 0 &&
                    <ChoiceSelector choices={this.state.choices}
                                    onClick={(id) => this.guess(id)}/>
                    }
                    {this.state.nowPlaying !== '' &&
                    <FadeController song={this.state.nowPlaying}
                                    points={points}
                                    pay={(seconds) => this.extend(seconds)}/>}
                </div>
                <div className="col-lg-7">
                    <div className="row">
                        <MessageList messages={this.state.messages}/>
                        <UserList users={this.state.users}/>
                        <AddMessage
                            messageAdded={(message) => this.dispatcher.sendMessage(message)}/>
                        <Playlist songs={this.state.playlist}
                                  songRemoved={(id) => this.removeSong(id)}
                                  songAdded={(data) => this.dispatcher.send(data, 'playlist_add')}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
