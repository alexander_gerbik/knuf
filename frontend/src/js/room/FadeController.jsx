import React from "react";

class FadeController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            seconds: 5,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    changeInterval(value) {
        this.setState({
            seconds: value,
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.pay(this.state.seconds);
    }

    render() {
        let toPay = this.state.seconds * payMultiplier;
        let enoughPoints = toPay <= this.props.points;

        return (
            <div className="card mb-3 bg-light">
                <div className="card-body">
                    <h5 className="card-title">Now playing:</h5>
                    <p className="card-text">{this.props.song}</p>
                    <input type="range" value={this.state.seconds}
                           onChange={(event) => this.changeInterval(event.target.value)}
                           min="5" max="60" step="1"/>{' '}
                    <p>Pay {toPay} points to increase song duration
                        by {this.state.seconds} seconds.</p>
                    <form onSubmit={this.handleSubmit} className="input-group">
                        <button type="submit" disabled={!enoughPoints}
                                className="btn btn-success">Pay
                        </button>
                    </form>
                </div>
            </div>

        );
    }
}

export default FadeController;
