import React from "react";

class Choice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    handleClick() {
        if (this.props.state === "enabled") {
            this.props.onClick();
        }
    }
    render() {
        let className = "card mb-3";
        if (this.props.state === "enabled") {
            className += " bg-light choice-enabled";
        } else if (this.props.state === "disabled") {
            className += " bg-light";
        } else if (this.props.state === "wrong") {
            className += " bg-danger";
        } else if (this.props.state === "right") {
            className += " bg-success";
        }
        return (
            <div className={className} onClick={() => this.handleClick()}>
                <div className="card-body">
                    <p className="card-text">{this.props.artist} - {this.props.name}</p>
                </div>
            </div>
        );
    }
}

export default Choice;
