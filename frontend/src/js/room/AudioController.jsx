import React from "react";

class AudioController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            playing: false,
            stopped: true,
        };
    }

    reInit() {
        this.setState({
            playing: false,
            stopped: true,
        });
    }

    play() {
        this.setState({
            playing: true,
            stopped: false,
        });
        this.props.playPressed();
    }

    pause() {
        this.setState({
            playing: false,
            stopped: false,
        });
        this.props.pausePressed();
    }

    stop() {
        this.setState({
            playing: false,
            stopped: true,
        });
        this.props.stopPressed();
    }

    componentDidMount() {
        if (this.props.onRef !== undefined) {
            this.props.onRef(this);
        }
    }

    componentWillUnmount() {
        if (this.props.onRef !== undefined) {
            this.props.onRef(undefined);
        }
    }

    render() {
        return (
            <React.Fragment>
                <button title="play"
                        disabled={this.state.playing}
                        onClick={() => this.play()}>
                    <i className="fas fa-play"></i>
                </button>
                <button title="pause"
                        disabled={!this.state.playing}
                        onClick={() => this.pause()}>
                    <i className="fas fa-pause"></i>
                </button>
                <button title="stop"
                        disabled={this.state.stopped}
                        onClick={() => this.stop()}>
                    <i className="fas fa-stop"></i>
                </button>
            </React.Fragment>
        );
    }
}

export default AudioController;
