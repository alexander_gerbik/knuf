import React from "react";
import Modal from "react-modal";
import {Spinner} from "../icons";
import {getSongSource} from "../utils";

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};

class FragmentSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false,
            loading: false,
            choices: [],
        };
        this.openModal = this.openModal.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        let self = this;
        $.get(userFragmentsUrl).done(function (data) {
            self.setState({
                choices: data.fragments,
                modalIsOpen: true,
                loading: false,
            });
        });
        this.setState({loading: true});
    }

    afterOpenModal() {
        // ref's are now sync'd and can be accessed.
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    fragmentSelected(fragment) {
        let songUrl = "http://myzuka.club/Song/" + fragment.id + "/a-b";
        getSongSource(songUrl, (data) => {
            fragment.url = data.url;
            this.props.onSelect(fragment);
        });
        this.closeModal();
    }

    renderFragment(fragment) {
        const key = `${fragment.id}-${fragment.begin}-${fragment.end}`;
        return (
            <a href="#" className="list-group-item list-group-item-action"
               key={key}
               onClick={() => this.fragmentSelected(fragment)}>
                {fragment.artist} - {fragment.name} {fragment.begin}-{fragment.end}
            </a>
        );
    }

    render() {
        const el = (this.state.loading) ? <Spinner/>
            : <button  className="btn btn-success" onClick={this.openModal}>add fragment</button>;
        let fragments = this.state.choices.map((el) => this.renderFragment(el));
        fragments = (fragments.length > 0) ? fragments
            : "You have no fragments. Add fragments in 'fragments' section.";
        return (
            <div>
                {el}
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel="Song Fragments"
                >
                    <h2>Your fragments</h2>
                    <div className="list-group">
                        {fragments}
                    </div>
                </Modal>
            </div>
        );
    }
}

export default FragmentSelect;
