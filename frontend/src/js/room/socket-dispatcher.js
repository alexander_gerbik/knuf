'use strict';

class SocketDispatcher {
    constructor(server) {
        this._websocket = new WebSocket(server);
        this._registry = {};
        this._dispatch = this._dispatch.bind(this);
        this._websocket.onmessage = this._dispatch;
    }

    set onerror(value) {
        this._websocket.onerror = value;
    }

    set onopen(value) {
        this._websocket.onopen = value;
    }

    set onclose(value) {
        this._websocket.onclose = value;
    }

    send(data, type) {
        let d = {};
        Object.assign(d, data);
        if (type !== undefined) {
            Object.assign(d, {'type': type});
        }
        this._websocket.send(JSON.stringify(d));
    }

    sendMessage(message) {
        this.send({'payload': message}, 'message');
    }

    on(type, callback) {
        if (type in this._registry) {
            console.warn(`callback for ${type} has been overriden`);
        }
        this._registry[type] = callback;
    }

    _dispatch(message) {
        let data = JSON.parse(message.data);
        let type = data.type;
        delete data.type;
        if (!(type in this._registry)) {
            console.log(`unknown message: ${message.data}`);
            return;
        }
        return this._registry[type](data);
    }
};

export default SocketDispatcher;
