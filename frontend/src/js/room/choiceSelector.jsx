import React from "react";

import Choice from './Choice';

class ChoiceSelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let choices = this.props.choices.map((choice) =>
            <Choice key={choice.id} onClick={() => this.props.onClick(choice.id)} {...choice}/>
        );
        return (
            <div>
                {choices}
            </div>
        );
    }
}

export default ChoiceSelector;
