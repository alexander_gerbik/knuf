import React from "react";
import {getCookie} from "../utils";
import SongInput from "./SongInput";
import FragmentSelect from "./FragmentSelect";

class Playlist extends React.Component {
    constructor(props) {
        super(props);
        this.handleError = this.handleError.bind(this);
    }

    handleError(data) {
        if (data.error === 'Banned') {
            $.notify({
                message: 'It seems that we are banned by myzuka.club. Sorry for that.',
            }, {
                type: 'info',
                "allow_dismiss": true,
            });
        } else {
            console.log(data);
        }
    }

    handleClick(id) {
        this.props.songRemoved(id);
    }

    render() {
        let songs = [];
        let ss = this.props.songs;
        for (let i in ss) {
            let key = ss[i].id;
            songs.push(
                <li key={key} id={'song-' + key}>
                    {ss[i].artist} - {ss[i].name}
                    <button type="button" className="close" aria-label="Close"
                            onClick={() => this.handleClick(key)}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </li>
            );
        }
        return (
            <div className="col-12 playlist">
                <ul>
                    {songs}
                </ul>
                <SongInput songAdded={this.props.songAdded} onError={this.handleError}/>
                <FragmentSelect onSelect={this.props.songAdded}/>
            </div>
        );
    }
}

export default Playlist;
