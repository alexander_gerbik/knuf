import React from "react";

import User from "./user";

class UserList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let users = this.props.users.map((u) => <User key={u.uid} {...u}/>);
        return (
            <div className="user-list col-4">
                {users}
            </div>
        );
    }
}

export default UserList;
