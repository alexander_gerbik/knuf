import React from "react";

import {Leader, Passive, Wrong, Right} from "../../icons";

class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    getIcon() {
        if (this.props.state === "leader") return (
            <Leader/>
        );
        if (this.props.state === "wrong") return (
            <Wrong/>
        );
        if (this.props.state === "right") return (
            <Right/>
        );
        return (
            <Passive/>
        );
    }

    render() {
        const link = `/users/user/${this.props.name}/`;
        const icon = this.getIcon();
        const points = Math.round(this.props.points);
        const score = Math.round(this.props.score);
        return (
            <div>
                {icon}{" "}
                <span title="points">{points}</span>{" - "}
                <span title="score">{score}</span>{" "}
                <a href={link}>{this.props.name}</a><br/>
            </div>
        );
    }
}

export default User;
