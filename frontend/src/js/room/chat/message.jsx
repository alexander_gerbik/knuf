import React from "react";

import Date from "../../Date";

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <Date timestamp={this.props.timestamp}>
                    {this.props.sender}: {this.props.payload}
                </Date>
                <br/>
            </React.Fragment>
        );
    }
}

export default Message;
