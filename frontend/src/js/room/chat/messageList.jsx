import React from "react";

import Message from "./message";

class MessageList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let messages = [];
        let ms = this.props.messages;
        for (let i in ms) {
            let key = ms[i].sender_id + '-' + ms[i].timestamp;
            messages.push(<Message key={key} {...ms[i]}/>);
        }
        return (
            <div className="message-list col-8">
                <div>
                    {messages}
                </div>
                <div style={{float: "left", clear: "both"}}
                     ref={(el) => {
                         this.messagesEnd = el;
                     }}>
                </div>
            </div>
        );
    }

    scrollToBottom() {
        this.messagesEnd.scrollIntoView({behavior: "smooth"});
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }
}

export default MessageList;
