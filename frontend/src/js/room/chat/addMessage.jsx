import React from "react";

class AddMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        this.props.messageAdded(this.state.value);
        this.setState({value: ''});
        event.preventDefault();
    }

    render() {
        return (
            <div className="add-message-block  col-lg-12">
                <form onSubmit={this.handleSubmit} className="input-group">
                    <input className="form-control" placeholder="Type your message here"
                           type="text" value={this.state.value} onChange={this.handleChange} />
                    <button type="submit" className="btn btn-success">Send</button>
                </form>
            </div>
        );
    }
}

export default AddMessage;
