import React from "react";
import {Muted, Unmuted} from "../icons";
import {setDefault} from "../utils";

class AudioComponent extends React.Component {
    constructor(props) {
        super(props);

        let muted = setDefault(localStorage, 'audio-muted', 'false') === 'true';
        let volume = Number.parseFloat(setDefault(localStorage, 'audio-volume', 1));

        this.state = {
            current: props.from,
            muted: muted,
            volume: volume,
        };
    }

    play() {
        this.audio.play();
        this.beginTime = window.performance.now() - this.audio.currentTime * 1000;
        this.playing = true;
    }

    pause() {
        this.audio.pause();
        this.playing = false;
    }

    stop() {
        this.audio.pause();
        this.audio.currentTime = this.props.from;
        this.setState({current: this.props.from});
        this.playing = false;
        if (this.props.onStop !== undefined) {
            this.props.onStop();
        }
    }


    componentDidMount() {
        this.audio = new Audio(this.props.songUrl);
        this.audio.muted = this.state.muted;
        this.audio.volume = this.state.volume;
        this.audio.currentTime = this.props.from;
        if (this.props.onRef !== undefined) {
            this.props.onRef(this);
        }
        this.timerID = setInterval(() => this.tick(), 10);
        if (!(this.props.autoStart === false)) {
            this.play();
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.songUrl !== prevProps.songUrl) {
            this.audio.pause();
            this.audio.src = this.props.songUrl;
            this.audio.load();
            this.audio.currentTime = this.props.from;
            if (this.playing) {
                this.play();
            }
        }
        if (!this.playing && this.props.from !== prevProps.from) {
            this.audio.currentTime = this.props.from;
            this.beginTime = window.performance.now() - this.audio.currentTime * 1000;
            this.setState({current: this.props.from});
        }
    }

    componentWillUnmount() {
        if (this.props.onRef !== undefined) {
            this.props.onRef(undefined);
        }
        this.pause();
        clearInterval(this.timerID);
    }

    tick() {
        if (this.playing) {
            let current = (window.performance.now() - this.beginTime) / 1000;
            this.setState({current: current});
            if (current >= this.props.to) {
                this.stop();
            }
        }
    }

    toggleMute() {
        let newState = !this.state.muted;
        this.audio.muted = newState;
        localStorage.setItem('audio-muted', newState);
        this.setState({muted: newState});
    }

    changeVolume(value) {
        this.setState({
            volume: value,
        });
        this.audio.volume = value;
        localStorage.setItem('audio-volume', this.state.volume);
    }

    render() {
        let current = formatTime(this.state.current);
        let to = formatTime(this.props.to);
        let min = this.props.minFrom === undefined ? '' : formatTime(this.props.minFrom) + '|';
        let max = (this.props.maxTo === null || this.props.maxTo === undefined)
            ? '' : '|' + formatTime(this.props.maxTo);

        return (
            <div className="card mb-3">
                <div className="card-body audio-component">
                    <span
                        className="float-left">{min}{current}/{to}{max}{' '}</span>
                    {this.props.allowSkip &&
                    <span className='ml-3 float-left' title="Skip to next song"
                          onClick={() => this.props.onSkip()}>
                            <i className="fas fa-forward"></i>
                        </span>}
                    <span className="ml-3 mute-container float-right"
                          onClick={() => this.toggleMute()}>
                     {this.state.muted ? <Muted/> : <Unmuted/>}
                    </span>
                    <input className="float-right" type="range"
                           min="0" max="1" step="0.01"
                           value={this.state.volume}
                           onChange={(event) => this.changeVolume(event.target.value)}/>
                </div>
            </div>
        );
    }
}

function formatTime(seconds) {
    let minutes = Math.floor(seconds / 60);
    seconds = Math.floor(seconds % 60);
    return pad(minutes, 2, '0') + ':' + pad(seconds, 2, '0');
}

function pad(str, width, padder) {
    padder = padder || ' ';
    str = '' + str;
    return str.length >= width ? str : new Array(width - str.length + 1).join(padder) + str;
}

export default AudioComponent;
