import React from "react";
import {getCookie, getSongSource} from "../utils";

class SongInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url: '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({url: event.target.value});
    }

    handleSubmit(event) {
        getSongSource(this.state.url, (data) => this.props.songAdded(data),
            (data) => {
                if (this.props.onError !== undefined) {
                    this.props.onError(data);
                }
            });
        this.setState({url: ''});
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} className="input-group">
                <div className="input-group">
                    <div className="input-group-prepend">
                        <span className="input-group-text">
                            <a href="http://myzuka.club/" target="_blank">
                                <i className="fas fa-external-link-alt form-control-feedback"></i>
                            </a>
                        </span>
                    </div>
                    <input className="form-control" type="text"
                           placeholder="myzuka.club/Song/<song-id>/<artist>-<name>"
                           value={this.state.url}
                           onChange={this.handleChange}/>
                    <button type="submit" className="btn btn-success">Add
                        song
                    </button>
                </div>
            </form>
        );
    }
}

export default SongInput;
