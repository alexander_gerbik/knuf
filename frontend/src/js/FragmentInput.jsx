import React from "react";
import TwoHandleRange from "./TwoHandleRange";

class FragmentInput extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={this.props.className}>
                <input type="number" step="0.01" min="0"
                       className="range-input"
                       max={this.props.right} value={this.props.left}
                       onChange={(e) => this.props.onChange(e.target.value, undefined)}/>
                <TwoHandleRange className="two-handle-range"
                                left={this.props.left}
                                right={this.props.right}
                                max={this.props.max}
                                onChange={(left, right) => this.props.onChange(left, right)}
                />
                <input type="number" step="0.01" min={this.props.left}
                       className="range-input"
                       max={this.props.max} value={this.props.right}
                       onChange={(e) => this.props.onChange(undefined, e.target.value)}/>
            </div>
        );
    }
}

export default FragmentInput;
