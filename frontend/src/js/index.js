import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import Game from './tic-tac-toe/game';
import App from './room/app';
import EditFragment from './EditFragment';
import SocketDispatcher from './room/socket-dispatcher';
import {getCookie, parseJwt, post} from './utils';

let roomElement = document.getElementById('room-root');
if (roomElement !== null) {
    let dispatcher = new SocketDispatcher(server);
    Modal.setAppElement(roomElement);

    dispatcher.onopen = function (event) {
        var jwt = getCookie('jwt_token');
        let token = parseJwt(jwt);
        dispatcher.sendMessage(jwt);

        ReactDOM.render(
            React.createElement(App, {dispatcher: dispatcher, id: token.id}),
            roomElement
        );
    };

    dispatcher.on('info', function (data) {
        $.notify({
            message: data['payload'],
        }, {
            type: 'info',
            "allow_dismiss": true,
        });
    });

    dispatcher.onerror = function (error) {
        console.log('WebSocket Error: ' + error);
    };

    dispatcher.onclose = function (event) {
        if (event.code == 1006) {
            roomElement.innerHTML = "<p>Such room doesn't exist.</p>";
        }
        console.log('Disconnected from ' + server);
    };
}

let tttElement = document.getElementById('tic-tac-toe-root');
if (tttElement !== null) {
    ReactDOM.render(React.createElement(Game), tttElement);
    Modal.setAppElement(tttElement);
}

let editFragmentElement = document.getElementById('edit-fragment');
if (editFragmentElement !== null) {
    function submitFragment(songId, begin, end) {
        let token = getCookie('csrftoken');

        post(location.href, {
            "song": songId,
            "begin": begin,
            "end": end,
            "csrfmiddlewaretoken": token,
        });
    }
    ReactDOM.render(
        React.createElement(EditFragment, {
            id: songId,
            begin: begin,
            end: end,
            onSubmit: submitFragment,
        }), editFragmentElement
    );
    Modal.setAppElement(editFragmentElement);
}

$(function () {
    if (localStorage.getItem('sidebar-visible') === null) {
        localStorage.setItem("sidebar-visible", "true");
    }

    $("#sidebar-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        let current = localStorage.getItem('sidebar-visible');
        let next = (current === "true") ? "false" : "true";
        localStorage.setItem("sidebar-visible", next);
    });
});
