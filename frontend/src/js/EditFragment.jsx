import React from "react";
import SongInput from "./room/SongInput";
import {getCookie, getSongSource} from "./utils";
import AudioComponent from "./room/AudioComponent";
import AudioController from "./room/AudioController";
import TwoHandleRange from "./TwoHandleRange";
import FragmentInput from "./FragmentInput";

class EditFragment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            begin: props.begin,
            end: props.end,
            duration: 0,
            url: '',
        };

        this.handleSongAdded = this.handleSongAdded.bind(this);
        this.handleError = this.handleError.bind(this);

        if (props.id !== null) {
            let songUrl = "http://myzuka.club/Song/" + props.id + "/a-b";
            getSongSource(songUrl, (data) => this.handleSongAdded(data), (data) => this.handleError(data));
        }
    }

    handleSongAdded(data) {
        this.setState({
            url: data.url,
            id: data.id,
            duration: data.duration,
            end: (this.state.end !== -1) ? this.state.end : data.duration,
            begin: (this.state.begin !== -1) ? this.state.begin : 0,
        });
    }

    handleError(data) {
        console.log(data);
    }

    discardSong() {
        this.setState({
            id: null,
            begin: -1,
            end: -1,
            duration: 0,
            url: '',
        });
    }

    changeRange(begin, end) {
        let b = (begin !== undefined && begin <= this.state.end) ? begin : this.state.begin;
        let e = (end !== undefined && end >= this.state.begin) ? end : this.state.end;
        this.setState({
            begin: b,
            end: e,
        });
    }

    submit() {
        this.props.onSubmit(this.state.id, this.state.begin, this.state.end);
    }

    render() {
        let discardStyle = {
            color: 'red',
        };

        let el = this.state.url === '' ?
            <SongInput songAdded={this.handleSongAdded}
                       onError={this.handleError}/> : (
                <div>
                    <AudioComponent from={this.state.begin}
                                    to={this.state.end}
                                    songUrl={this.state.url}
                                    minFrom={this.state.begin}
                                    maxTo={this.state.duration}
                                    autoStart={false}
                                    onRef={(ref) => this.audio = ref}
                                    onStop={() => this.audioController.reInit()}
                    />
                    <AudioController playPressed={() => this.audio.play()}
                                     pausePressed={() => this.audio.pause()}
                                     stopPressed={() => this.audio.stop()}
                                     onRef={(ref) => this.audioController = ref}
                    />
                    <FragmentInput className="fragment-input"
                                   left={this.state.begin}
                                   right={this.state.end}
                                   max={this.state.duration}
                                   onChange={(left, right) => this.changeRange(left, right)}
                    />
                    <button title="choose another song" style={discardStyle}
                            onClick={() => this.discardSong()}>
                        <i className="fas fa-times"></i>
                    </button>
                    <div>
                        <button onClick={() => this.submit()} className="btn btn-success">Save</button>
                    </div>
                </div>
            );
        return el;
    }
}

export default EditFragment;
