import React from "react";
import moment from "moment";

function formatDate(timestamp) {
    let date = moment(timestamp);
    return date.fromNow();
}

class Date extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: formatDate(this.props.timestamp)};
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: formatDate(this.props.timestamp),
        });
    }

    render() {
        return (
            <span title={this.state.date}>
                {this.props.children}
            </span>
        );
    }
}

export default Date;
