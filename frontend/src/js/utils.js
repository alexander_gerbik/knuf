export function parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
};

export function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
}

export function post(url, data) {
    var form = document.createElement('form');
    document.body.appendChild(form);
    form.method = 'post';
    form.action = url;
    for (var name in data) {
        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = name;
        input.value = data[name];
        form.appendChild(input);
    }
    form.submit();
}

export function setDefault(storage, key, defaultValue) {
    let item = storage.getItem(key);
    if (item !== null) return item;
    storage.setItem(key, defaultValue);
    return defaultValue;
}

export function getSongSource(songUrl, onSuccess, onError) {
    let csrftoken = getCookie('csrftoken');
    $.ajax({
        url: requestSongUrl,
        method: "POST",
        data: {'url': songUrl},
        headers: {'X-CSRFToken': csrftoken},
    }).done(function (data) {
        (!('error' in data)) ? onSuccess(data) : onError !== undefined && onError(data);
    });

}
