import React from 'react';

function Square(props) {
    let className = "square";
    className += (props.highlight ? " highlight" : "");
    className += (props.current ? " current" : "");
    return (
      <button className={className} onClick={props.onClick}>
        {props.value}
      </button>
    );
};

export default Square;
