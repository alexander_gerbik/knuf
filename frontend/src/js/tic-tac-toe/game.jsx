import React from 'react';
import Board from './board';
import calculateWinner from './calculateWinner';

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: new Array(9).fill(null),
        move: null,
      }],
      stepNumber: 0,
    };
  }
  
  xIsNext() {
    return (this.state.stepNumber % 2) === 0;
  }
  
  jumpTo(step) {
    this.setState({
      stepNumber: step,
    });
  }
  
  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.xIsNext() ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares,
        move: i,
      }]),
      stepNumber: history.length,
    });
  }
  
  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const combination = calculateWinner(current.squares);
    const winner = combination === null ? null : current.squares[combination[0]];
    
    const moves = history.map((step, move) => {
      let desc = move ?
            'Go to move #' + move :
      'Go to game start';
      desc = this.state.stepNumber === move ? <b>{desc}</b> : desc;
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
            </li>
          );
    });
    
    let status;
    if (winner) {
      status = 'Winner: ' + winner;
    } else if (current.squares.every(el => el !== null)) {
      status = 'Draw!';
    } else {
      status = 'Next player: ' + (this.xIsNext() ? 'X' : 'O');
    }
    
    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={(i) => this.handleClick(i)}
            combination={combination}
            move={current.move}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
};

export default Game;
