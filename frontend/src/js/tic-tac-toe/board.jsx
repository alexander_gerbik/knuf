import React from 'react';
import Square from './square';

class Board extends React.Component {
  renderSquare(i) {
    const comb = this.props.combination;
    const highlight = comb !== null && (i === comb[0] || i === comb[1] || i === comb[2]);
    return <Square
             value={this.props.squares[i]}
             onClick={() => this.props.onClick(i)}
             highlight={highlight}
             current={this.props.move === i}
           />;
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
};

export default Board;
