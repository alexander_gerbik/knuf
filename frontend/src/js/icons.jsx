import React from "react";

export class Leader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <span title="leader"><i className="fas fa-user"></i></span>
        );
    }
}

export class Wrong extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <span title="guessed wrong"><i className="fas fa-times"></i></span>
        );
    }
}

export class Right extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <span title="guessed right"><i className="fas fa-check"></i></span>
        );
    }
}

export class Passive extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <span title="have not guessed yet"><i className="fas fa-question"></i></span>
        );
    }
}

export class Muted extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <span title="Unmute"><i className="fas fa-volume-off"></i></span>
        );
    }
}

export class Unmuted extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <span title="Mute"><i className="fas fa-volume-up"></i></span>
        );
    }
}

export class Spinner extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <span title="Loading..."><i className="fas fa-square spinner"></i></span>
        );
    }
}
